<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * OfOficiosInsumosFixture
 *
 */
class OfOficiosInsumosFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'co_usuario_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'of_oficio_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'cat_insumo_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'fecha_entrega' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'cantidad' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'precio_unitario' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_of_oficios_insumos_cat_insumos1_idx' => ['type' => 'index', 'columns' => ['cat_insumo_id'], 'length' => []],
            'fk_of_oficios_insumos_co_usuarios1_idx' => ['type' => 'index', 'columns' => ['co_usuario_id'], 'length' => []],
            'fk_of_oficios_insumos_of_oficios1_idx' => ['type' => 'index', 'columns' => ['of_oficio_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_of_oficios_insumos_cat_insumos1' => ['type' => 'foreign', 'columns' => ['cat_insumo_id'], 'references' => ['cat_insumos', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_of_oficios_insumos_co_usuarios1' => ['type' => 'foreign', 'columns' => ['co_usuario_id'], 'references' => ['co_usuarios', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_of_oficios_insumos_of_oficios1' => ['type' => 'foreign', 'columns' => ['of_oficio_id'], 'references' => ['of_oficios', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => '01343bc5-0c8a-4753-84f6-f259e3215524',
            'co_usuario_id' => '6fb14618-912a-4b71-9c20-7ea3210202c3',
            'of_oficio_id' => 'c7d423c2-c197-4e3c-aeec-166151f24755',
            'cat_insumo_id' => '292a8223-bb45-47b4-ab58-ba89ddc03f9b',
            'fecha_entrega' => '2018-07-24',
            'cantidad' => 1,
            'precio_unitario' => 1,
            'created' => '2018-07-24 17:05:38',
            'modified' => '2018-07-24 17:05:38'
        ],
    ];
}
