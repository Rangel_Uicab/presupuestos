<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatFormatosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatFormatosTable Test Case
 */
class CatFormatosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CatFormatosTable
     */
    public $CatFormatos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cat_formatos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CatFormatos') ? [] : ['className' => CatFormatosTable::class];
        $this->CatFormatos = TableRegistry::get('CatFormatos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatFormatos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
