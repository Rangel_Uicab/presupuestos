<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatProgramasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatProgramasTable Test Case
 */
class CatProgramasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CatProgramasTable
     */
    public $CatProgramas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cat_programas',
        'app.cat_unidades',
        'app.cat_direcciones',
        'app.co_usuarios',
        'app.co_grupos',
        'app.co_menus',
        'app.co_grupos_co_menus',
        'app.co_permisos',
        'app.co_grupos_co_permisos',
        'app.co_usuarios_co_grupos',
        'app.of_oficios',
        'app.cat_insumos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CatProgramas') ? [] : ['className' => CatProgramasTable::class];
        $this->CatProgramas = TableRegistry::get('CatProgramas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatProgramas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
