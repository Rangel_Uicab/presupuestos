<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatPersonasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatPersonasTable Test Case
 */
class CatPersonasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CatPersonasTable
     */
    public $CatPersonas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cat_personas',
        'app.cat_municipios',
        'app.cat_estados',
        'app.cat_localidades',
        'app.cat_unidades',
        'app.cat_tipos_unidades_centrales',
        'app.cat_jurisdicciones',
        'app.co_usuarios',
        'app.co_grupos',
        'app.co_menus',
        'app.co_grupos_co_menus',
        'app.co_permisos',
        'app.co_grupos_co_permisos',
        'app.co_usuarios_co_grupos',
        'app.cat_personas_cat_municipios'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CatPersonas') ? [] : ['className' => 'App\Model\Table\CatPersonasTable'];
        $this->CatPersonas = TableRegistry::get('CatPersonas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatPersonas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
