<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatSexosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatSexosTable Test Case
 */
class CatSexosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CatSexosTable
     */
    public $CatSexos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cat_sexos',
        'app.cat_personas',
        'app.cat_estatus',
        'app.cat_zonas',
        'app.cat_municipios',
        'app.cat_estados',
        'app.cat_localidades',
        'app.cat_unidades',
        'app.cat_tipos_unidades_centrales',
        'app.co_usuarios',
        'app.co_grupos',
        'app.co_menus',
        'app.co_grupos_co_menus',
        'app.co_permisos',
        'app.co_grupos_co_permisos',
        'app.co_usuarios_co_grupos',
        'app.cat_zonas_cat_municipios',
        'app.ope_fotos_personas',
        'app.ope_seguimientos',
        'app.cat_personas_cat_municipios'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CatSexos') ? [] : ['className' => 'App\Model\Table\CatSexosTable'];
        $this->CatSexos = TableRegistry::get('CatSexos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatSexos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
