<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OfOficiosInsumosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OfOficiosInsumosTable Test Case
 */
class OfOficiosInsumosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OfOficiosInsumosTable
     */
    public $OfOficiosInsumos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.of_oficios_insumos',
        'app.co_usuarios',
        'app.co_grupos',
        'app.co_menus',
        'app.co_grupos_co_menus',
        'app.co_permisos',
        'app.co_grupos_co_permisos',
        'app.co_usuarios_co_grupos',
        'app.of_oficios',
        'app.cat_unidades',
        'app.cat_direcciones',
        'app.cat_programas',
        'app.cat_insumos',
        'app.cat_fuentes_financiamientos',
        'app.cat_estatus',
        'app.cat_programa',
        'app.cat_tipos_oficios'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OfOficiosInsumos') ? [] : ['className' => OfOficiosInsumosTable::class];
        $this->OfOficiosInsumos = TableRegistry::get('OfOficiosInsumos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OfOficiosInsumos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
