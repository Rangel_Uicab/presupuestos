<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatInsumosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatInsumosTable Test Case
 */
class CatInsumosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CatInsumosTable
     */
    public $CatInsumos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cat_insumos',
        'app.co_usuarios',
        'app.co_grupos',
        'app.co_menus',
        'app.co_grupos_co_menus',
        'app.co_permisos',
        'app.co_grupos_co_permisos',
        'app.co_usuarios_co_grupos',
        'app.cat_fuentes_financiamientos',
        'app.cat_programas',
        'app.cat_unidades',
        'app.cat_direcciones',
        'app.of_oficios',
        'app.of_oficios_insumos',
        'app.cat_estatus'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CatInsumos') ? [] : ['className' => CatInsumosTable::class];
        $this->CatInsumos = TableRegistry::get('CatInsumos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatInsumos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
