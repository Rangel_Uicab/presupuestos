<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatTiposOficiosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatTiposOficiosTable Test Case
 */
class CatTiposOficiosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CatTiposOficiosTable
     */
    public $CatTiposOficios;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cat_tipos_oficios',
        'app.of_oficios',
        'app.cat_unidades',
        'app.cat_direcciones',
        'app.cat_programas',
        'app.cat_insumos',
        'app.co_usuarios',
        'app.co_grupos',
        'app.co_menus',
        'app.co_grupos_co_menus',
        'app.co_permisos',
        'app.co_grupos_co_permisos',
        'app.co_usuarios_co_grupos',
        'app.cat_fuentes_financiamientos',
        'app.cat_estatus',
        'app.of_oficios_insumos',
        'app.cat_programa'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CatTiposOficios') ? [] : ['className' => CatTiposOficiosTable::class];
        $this->CatTiposOficios = TableRegistry::get('CatTiposOficios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatTiposOficios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
