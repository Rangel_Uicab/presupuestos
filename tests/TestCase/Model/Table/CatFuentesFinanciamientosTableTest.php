<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatFuentesFinanciamientosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatFuentesFinanciamientosTable Test Case
 */
class CatFuentesFinanciamientosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CatFuentesFinanciamientosTable
     */
    public $CatFuentesFinanciamientos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cat_fuentes_financiamientos',
        'app.cat_insumos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CatFuentesFinanciamientos') ? [] : ['className' => CatFuentesFinanciamientosTable::class];
        $this->CatFuentesFinanciamientos = TableRegistry::get('CatFuentesFinanciamientos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatFuentesFinanciamientos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
