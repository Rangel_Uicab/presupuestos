<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\App;

/**
 * CatInsumos Controller
 *
 * @property \App\Model\Table\CatInsumosTable $CatInsumos
 *
 * @method \App\Model\Entity\CatInsumo[] paginate($object = null, array $settings = [])
 */
class CatInsumosController extends AppController
{
    public $paginate = array();


    public function getData()
    {
         $aColumns = array
                        (
                          'CatInsumos.clave_partida',
                          'CatInsumos.insumo',
                          'CatInsumos.descripcion_insumo',
                          'CatInsumos.indice_accion_especifica',

                          );

        $sIndexColumn = "CatInsumos.id";

        //Verificamos que nos enviaron la cantidad de registros que se requieren por pagina
        if(isset($this->request->query['iDisplayLength']))
        {
        	$this->paginate['maxLimit'] =$this->request->query['iDisplayLength'];
        	$this->paginate['limit'] =$this->request->query['iDisplayLength'];
        }

        //Verificamos si nos enviaron la pagina que desean visualizar
        if(isset($this->request->query['iDisplayStart']))
        {
            //Se realiza la division para obtener el numero de pagina
            $this->paginate['page'] = ($this->request->query['iDisplayStart']/$this->request->query['iDisplayLength'])+1;
        }
        //Verificamos si se envio algun orden de columna en especifico
        if(isset($this->request->query['iSortCol_0']))
        {
            $orden = [];
            for ( $i=0 ; $i < intval( $this->request->query['iSortingCols'] ) ; $i++ )
            {
                if ( $this->request->query[ 'bSortable_'.intval($this->request->query['iSortCol_'.$i]) ] == "true" )
                {
                    $column = $aColumns[ intval( $this->request->query['iSortCol_'.$i] ) ];
                	$order = ($this->request->query['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
                	$orden = [ $column => $order];
                }
            }
            //Si la cadena no esta vacia se la agregamos a las opciones del paginador en la opcion "order"
            if ( !empty($orden))
            {
                $this->paginate['order'] = $orden;
            }
        }
        //Revisamos si se envio el filtro para todos los campos
        $conditions = array();
        if ( isset($this->request->query['sSearch']) && $this->request->query['sSearch'] != "" )
        {
            for ( $i=0 ; $i < count($aColumns) ; $i++ )
            {
                $conditions[][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
            }
            //Si el arreglo de condiciones no esta vacio, lo pasamos a las opciones del Paginador con el operador OR
            if(!empty($conditions))
            {
                $this->paginate['conditions']['OR'] = $conditions;
            }
        }
        //Verificamos si se envio algun filtro de campo especifico
        for ( $i=0 ; $i < count($aColumns) ; $i++ )
        {
            if ( isset($this->request->query['bSearchable_'.$i]) && $this->request->query['bSearchable_'.$i] == "true" && ($this->request->query['sSearch_'.$i] != '' || $this->request->query['sSearch'] != '') )
            {
                if(!empty($this->request->query['sSearch']))
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
                else
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch_'.$i].'%';
            }
        }

        if(!empty($this->paginate['conditions']))
        {
            $query = $this->CatInsumos->find()->where($this->paginate['conditions']);

             $this->paginate['contain'] = ['CatFuentesFinanciamientos', 'CatProgramas', 'CatEstatus'];
            $catInsumos = $this->paginate($query);
        }
        else
        {
        	$this->paginate['contain'] = ['CatFuentesFinanciamientos', 'CatProgramas', 'CatEstatus'];

            $catInsumos = $this->paginate('CatInsumos');
        }

        //Numero total de registros
        $iTotalDisplayRecords = $this->request->params['paging']['CatInsumos']['count'];

        //Numero de registros encontrados
        $iTotalRecords = $this->request->params['paging']['CatInsumos']['current'];

        //sEcho
        $sEcho = intval($this->request->query['sEcho']);

        //Datos para la tabla
        $aaData = array();

        //Cargamos los Helper para armar los links de acciones
        $View = new \App\View\AppView();
        App::classname('Html', 'View/Helper', 'Helper');
        $Html = $View->loadHelper('Html');
        $Form = $View->loadHelper('Form');

        $i = 0;
        foreach($catInsumos as $catInsumo)
        {

            $actions = "<div class='btn-group' role='group'>";
                $actions .= $Html->link("<i class='icon md-eye' aria-hidden='true'></i>",array('action'=>'view',$catInsumo->id),array('escape'=>false,'class'=>"btn btn-default"));
                $actions .= $Html->link("<i class='icon md-edit' aria-hidden='true'></i>",array('action'=>'edit',$catInsumo->id),array('escape'=>false,'class'=>"btn btn-default"));
                $actions .= $Form->postLink("<i class='icon md-delete' aria-hidden='true'></i>", ['action' => 'delete',$catInsumo->id], ['escape'=>false,'class'=>"btn btn-danger",'confirm' => __('Realmente desea eliminar el registro con el Id # {0}?', $catInsumo->id)]);
            $actions .="</div>";
          		$aaData[$i][] = $catInsumo->has('cat_fuentes_financiamiento') ? $catInsumo->cat_fuentes_financiamiento->name : '';
              $aaData[$i][] = $catInsumo->has('cat_programa') ? $catInsumo->cat_programa->name : '';
          		$aaData[$i][] = $catInsumo->indice_accion_especifica;
          		$aaData[$i][] = $catInsumo->clave_partida;
          		$aaData[$i][] = $catInsumo->insumo;
              $aaData[$i][] = $catInsumo->descripcion_insumo;
          		$aaData[$i][] = $catInsumo->unidad_medida;
          		$aaData[$i][] = number_format($catInsumo->cantidad);
          		$aaData[$i][] = number_format($catInsumo->precio_unitario,2);
          		$aaData[$i][] = number_format($catInsumo->total,2);
          		$aaData[$i][] = $catInsumo->estatus;
              $aaData[$i][] = $catInsumo->has('cat_estatus') ? '<span class="'.$catInsumo->cat_estatus->clase.'">'.$catInsumo->cat_estatus->name.'</span>' : '';
	            $aaData[$i][] = $actions;
            $i++;
        }
        //Enviamos y serializamos en JSON todas la variables requeridas por el jquery.dataTable
        $this->set(compact('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_serialize',array('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_jsonp',true);
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
    }

    /**
     * View method
     *
     * @param string|null $id Cat Insumo id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $catInsumo = $this->CatInsumos->get($id, [
            'contain' => ['CoUsuarios', 'CatFuentesFinanciamientos', 'CatProgramas', 'CatEstatus', 'OfOficiosInsumos'=>['OfOficios']]
        ]);

        $this->set('catInsumo', $catInsumo);
        $this->set('_serialize', ['catInsumo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $catInsumo = $this->CatInsumos->newEntity();
        if ($this->request->is('post')) {
            $catInsumo = $this->CatInsumos->patchEntity($catInsumo, $this->request->getData());
            $catInsumo->co_usuario_id = $this->request->session()->read('Auth.User.id');
            if ($this->CatInsumos->save($catInsumo))
            {

                $this->Flash->flash('Registro guardado.', ['params'=>['type'=>'info']]);

                return $this->redirect(['action' => 'add']);
            }
			 	$this->Flash->flash('El Registro no pudo ser guardado.', ['params'=>['type'=>'danger']]);

        }
        $catFuentesFinanciamientos = $this->CatInsumos->CatFuentesFinanciamientos->find('list', ['limit' => 200]);
        $catProgramas = $this->CatInsumos->CatProgramas->find('list', ['limit' => 200]);
        $catEstatus = $this->CatInsumos->CatEstatus->find('list', ['limit' => 200]);
        $this->set(compact('catInsumo', 'coUsuarios', 'catFuentesFinanciamientos', 'catProgramas', 'catEstatus'));
        $this->set('_serialize', ['catInsumo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cat Insumo id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $catInsumo = $this->CatInsumos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $catInsumo = $this->CatInsumos->patchEntity($catInsumo, $this->request->getData());
            if ($this->CatInsumos->save($catInsumo))
            {
            	$this->Flash->flash('Registro actualizado correctamente.', ['params'=>['type'=>'info']]);
                return $this->redirect(['action' => 'index']);
            }
           		$this->Flash->flash('El registro no se pudo actualizar correctamente. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
    }
        $coUsuarios = $this->CatInsumos->CoUsuarios->find('list', ['limit' => 200]);
        $catFuentesFinanciamientos = $this->CatInsumos->CatFuentesFinanciamientos->find('list', ['limit' => 200]);
        $catProgramas = $this->CatInsumos->CatProgramas->find('list', ['limit' => 200]);
        $catEstatus = $this->CatInsumos->CatEstatus->find('list', ['limit' => 200]);
        $this->set(compact('catInsumo', 'coUsuarios', 'catFuentesFinanciamientos', 'catProgramas', 'catEstatus'));
        $this->set('_serialize', ['catInsumo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cat Insumo id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $catInsumo = $this->CatInsumos->get($id);
        $catInsumo->activo = 0;
        if ($this->CatInsumos->save($catInsumo))
        {
        	$this->Flash->flash('Registro eliminado correctamente.', ['params'=>['type'=>'info']]);
        }
        else
        {
			$this->Flash->flash('El registro no pudo ser eliminado. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getInsumosPorPrograma()
    {
     if ($this->request->is('post'))
     {
       $catInsumos = $this->CatInsumos->find('list',['conditions'=>['CatInsumos.cat_programa_id'=>$this->request->data['id']]]);
     }
     $this->set(compact('catInsumos'));
     $this->set('_serialize',['catInsumos']);
     }

     public function getInfoInsumo()
     {
         $catInsumo = $this->CatInsumos->find()->contain(['CatFuentesFinanciamientos', 'CatProgramas', 'OfOficiosInsumos'=>['OfOficios']])->where(['CatInsumos.id'=>$this->request->data['id']])->first();
         $this->set('catInsumo', $catInsumo);
         $this->set('_serialize', ['catInsumo']);
     }

}
