<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\App;

/**
 * CatUnidades Controller
 *
 * @property \App\Model\Table\CatUnidadesTable $CatUnidades
 *
 * @method \App\Model\Entity\CatUnidade[] paginate($object = null, array $settings = [])
 */
class CatUnidadesController extends AppController
{
    public $paginate = array();

    
    public function getData()
    {
         $aColumns = array
                        (
                            'CatUnidades.id',
                                'CatUnidades.cat_direccione_id',
                                'CatUnidades.cat_unidad_padre_id',
                                'CatUnidades.created',
                                'CatUnidades.name',
                                'CatUnidades.nombre_corto',
                                'CatUnidades.modified',
                                );
        
        $sIndexColumn = "CatUnidades.id";
        
        //Verificamos que nos enviaron la cantidad de registros que se requieren por pagina
        if(isset($this->request->query['iDisplayLength']))
        {
        	$this->paginate['maxLimit'] =$this->request->query['iDisplayLength'];
        	$this->paginate['limit'] =$this->request->query['iDisplayLength'];        	
        }
        
        //Verificamos si nos enviaron la pagina que desean visualizar
        if(isset($this->request->query['iDisplayStart']))
        {
            //Se realiza la division para obtener el numero de pagina
            $this->paginate['page'] = ($this->request->query['iDisplayStart']/$this->request->query['iDisplayLength'])+1;
        }
        //Verificamos si se envio algun orden de columna en especifico
        if(isset($this->request->query['iSortCol_0']))
        {
            $orden = [];
            for ( $i=0 ; $i < intval( $this->request->query['iSortingCols'] ) ; $i++ )
            {
                if ( $this->request->query[ 'bSortable_'.intval($this->request->query['iSortCol_'.$i]) ] == "true" )
                {
                    $column = $aColumns[ intval( $this->request->query['iSortCol_'.$i] ) ];
                	$order = ($this->request->query['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
                	$orden = [ $column => $order];
                }
            }
            //Si la cadena no esta vacia se la agregamos a las opciones del paginador en la opcion "order"
            if ( !empty($orden))
            {
                $this->paginate['order'] = $orden;
            }
        }
        //Revisamos si se envio el filtro para todos los campos
        $conditions = array();
        if ( isset($this->request->query['sSearch']) && $this->request->query['sSearch'] != "" )
        {
            for ( $i=0 ; $i < count($aColumns) ; $i++ )
            {
                $conditions[][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
            }
            //Si el arreglo de condiciones no esta vacio, lo pasamos a las opciones del Paginador con el operador OR
            if(!empty($conditions))
            {
                $this->paginate['conditions']['OR'] = $conditions;
            }
        }
        //Verificamos si se envio algun filtro de campo especifico
        for ( $i=0 ; $i < count($aColumns) ; $i++ )
        {
            if ( isset($this->request->query['bSearchable_'.$i]) && $this->request->query['bSearchable_'.$i] == "true" && ($this->request->query['sSearch_'.$i] != '' || $this->request->query['sSearch'] != '') )
            {
                if(!empty($this->request->query['sSearch']))
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
                else
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch_'.$i].'%';
            }
        }
        
        if(!empty($this->paginate['conditions']))
        {
            $query = $this->CatUnidades->find()->where($this->paginate['conditions']);
            
             $this->paginate['contain'] = ['CatDirecciones', 'CatUnidadesPadres'];
            $catUnidades = $this->paginate($query);  
        }
        else
        {
        	$this->paginate['contain'] = ['CatDirecciones', 'CatUnidadesPadres'];
            
            $catUnidades = $this->paginate('CatUnidades');
        }
        
        //Numero total de registros
        $iTotalDisplayRecords = $this->request->params['paging']['CatUnidades']['count'];

        //Numero de registros encontrados
        $iTotalRecords = $this->request->params['paging']['CatUnidades']['current'];

        //sEcho
        $sEcho = intval($this->request->query['sEcho']);

        //Datos para la tabla
        $aaData = array();

        //Cargamos los Helper para armar los links de acciones
        $View = new \App\View\AppView();
        App::classname('Html', 'View/Helper', 'Helper');
        $Html = $View->loadHelper('Html');
        $Form = $View->loadHelper('Form');
        
        $i = 0;
        foreach($catUnidades as $catUnidade)
        {

            $actions = "<div class='btn-group' role='group'>";
                //$actions .= $Html->link("<i class='icon md-eye' aria-hidden='true'></i>",array('action'=>'view',$catUnidade->id),array('escape'=>false,'class'=>"btn btn-default"));
                $actions .= $Html->link("<i class='icon md-edit' aria-hidden='true'></i>",array('action'=>'edit',$catUnidade->id),array('escape'=>false,'class'=>"btn btn-default"));
                $actions .= $Form->postLink("<i class='icon md-delete' aria-hidden='true'></i>", ['action' => 'delete',$catUnidade->id], ['escape'=>false,'class'=>"btn btn-danger",'confirm' => __('Realmente desea eliminar el registro con el Id # {0}?', $catUnidade->id)]);
            $actions .="</div>";
            	$aaData[$i][] = $catUnidade->id;
				$aaData[$i][] = $catUnidade->has('cat_direccione') ? $catUnidade->cat_direccione->name : ' ';
				$aaData[$i][] = $catUnidade->has('cat_unidades_padre') ? $catUnidade->cat_unidades_padre->name : ' ';
				$aaData[$i][] = $catUnidade->name;
				$aaData[$i][] = $catUnidade->nombre_corto;
				$aaData[$i][] = $catUnidade->created;
	            $aaData[$i][] = $actions;
            $i++;
        }
        //Enviamos y serializamos en JSON todas la variables requeridas por el jquery.dataTable
        $this->set(compact('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_serialize',array('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_jsonp',true);
    }
    
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
    }

    /**
     * View method
     *
     * @param string|null $id Cat Unidade id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $catUnidade = $this->CatUnidades->get($id, [
            'contain' => ['CatDirecciones', 'CatUnidadesPadres', 'CatProgramas', 'CoUsuarios', 'OfOficios']
        ]);

        $this->set('catUnidade', $catUnidade);
        $this->set('_serialize', ['catUnidade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $catUnidade = $this->CatUnidades->newEntity();
        if ($this->request->is('post')) {
            $catUnidade = $this->CatUnidades->patchEntity($catUnidade, $this->request->getData());
            if ($this->CatUnidades->save($catUnidade)) 
            {
               
                $this->Flash->flash('Registro guardado.', ['params'=>['type'=>'info']]);

                return $this->redirect(['action' => 'index']);
            }
			 	$this->Flash->flash('El Registro no pudo ser guardado.', ['params'=>['type'=>'danger']]);
		
        }
        $catDirecciones = $this->CatUnidades->CatDirecciones->find('list', ['limit' => 200]);
		$catUnidades = $this->CatUnidades->find('list', ['conditions'=>['CatUnidades.cat_unidad_padre_id IS NULL'],'limit' => 200]);
        $this->set(compact('catUnidade', 'catDirecciones', 'catUnidades'));
        $this->set('_serialize', ['catUnidade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cat Unidade id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $catUnidade = $this->CatUnidades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $catUnidade = $this->CatUnidades->patchEntity($catUnidade, $this->request->getData());
            if ($this->CatUnidades->save($catUnidade)) 
            {
            	$this->Flash->flash('Registro actualizado correctamente.', ['params'=>['type'=>'info']]);
                return $this->redirect(['action' => 'index']);
            }
           		$this->Flash->flash('El registro no se pudo actualizar correctamente. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
    }
        $catDirecciones = $this->CatUnidades->CatDirecciones->find('list', ['limit' => 200]);
		$catUnidades = $this->CatUnidades->find('list', ['conditions'=>['CatUnidades.cat_unidad_padre_id IS NULL'],'limit' => 200]);
        $this->set(compact('catUnidade', 'catDirecciones', 'catUnidades'));
        $this->set('_serialize', ['catUnidade']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cat Unidade id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $catUnidade = $this->CatUnidades->get($id);
        if ($this->CatUnidades->delete($catUnidade)) 
        {
        	$this->Flash->flash('Registro eliminado correctamente.', ['params'=>['type'=>'info']]);
        } 
        else 
        {
			$this->Flash->flash('El registro no pudo ser eliminado. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
        }

        return $this->redirect(['action' => 'index']);
    }
}
