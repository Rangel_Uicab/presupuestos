<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\App;
use Cake\ORM\TableRegistry;

/**
 * OfOficiosInsumos Controller
 *
 * @property \App\Model\Table\OfOficiosInsumosTable $OfOficiosInsumos
 *
 * @method \App\Model\Entity\OfOficiosInsumo[] paginate($object = null, array $settings = [])
 */
class OfOficiosInsumosController extends AppController
{
    public $paginate = array();


    public function getData()
    {
         $aColumns = array
                        (
                                'OfOficios.num_oficio',
                                'CatInsumos.insumo'
                                );

        $sIndexColumn = "OfOficiosInsumos.id";

        //Verificamos que nos enviaron la cantidad de registros que se requieren por pagina
        if(isset($this->request->query['iDisplayLength']))
        {
        	$this->paginate['maxLimit'] =$this->request->query['iDisplayLength'];
        	$this->paginate['limit'] =$this->request->query['iDisplayLength'];
        }

        //Verificamos si nos enviaron la pagina que desean visualizar
        if(isset($this->request->query['iDisplayStart']))
        {
            //Se realiza la division para obtener el numero de pagina
            $this->paginate['page'] = ($this->request->query['iDisplayStart']/$this->request->query['iDisplayLength'])+1;
        }
        //Verificamos si se envio algun orden de columna en especifico
        if(isset($this->request->query['iSortCol_0']))
        {
            $orden = [];
            for ( $i=0 ; $i < intval( $this->request->query['iSortingCols'] ) ; $i++ )
            {
                if ( $this->request->query[ 'bSortable_'.intval($this->request->query['iSortCol_'.$i]) ] == "true" )
                {
                    $column = $aColumns[ intval( $this->request->query['iSortCol_'.$i] ) ];
                	$order = ($this->request->query['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
                	$orden = [ $column => $order];
                }
            }
            //Si la cadena no esta vacia se la agregamos a las opciones del paginador en la opcion "order"
            if ( !empty($orden))
            {
                $this->paginate['order'] = $orden;
            }
        }
        //Revisamos si se envio el filtro para todos los campos
        $conditions = array();
        if ( isset($this->request->query['sSearch']) && $this->request->query['sSearch'] != "" )
        {
            for ( $i=0 ; $i < count($aColumns) ; $i++ )
            {
                $conditions[][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
            }
            //Si el arreglo de condiciones no esta vacio, lo pasamos a las opciones del Paginador con el operador OR
            if(!empty($conditions))
            {
                $this->paginate['conditions']['OR'] = $conditions;
            }
        }
        //Verificamos si se envio algun filtro de campo especifico
        for ( $i=0 ; $i < count($aColumns) ; $i++ )
        {
            if ( isset($this->request->query['bSearchable_'.$i]) && $this->request->query['bSearchable_'.$i] == "true" && ($this->request->query['sSearch_'.$i] != '' || $this->request->query['sSearch'] != '') )
            {
                if(!empty($this->request->query['sSearch']))
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
                else
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch_'.$i].'%';
            }
        }

        if($this->request->session()->check('FiltroInsumosOficios'))
        {
          $Filtro = $this->request->session()->read('FiltroInsumosOficios');


          if(!empty($Filtro['cat_fuentes_financiamiento_id']))
          {
            $this->paginate['conditions'][] = [
                                              'OfOficiosInsumos.cat_insumo_id IN
                                              (
                                                  SELECT
                                                    ci.id
                                                  FROM
                                                    cat_insumos ci
                                                  WHERE
                                                    ci.cat_fuentes_financiamiento_id = "'.$Filtro['cat_fuentes_financiamiento_id'].'"
                                              )'
                                            ];
          }
          if(!empty($Filtro['cat_programa_id']))
          {
            $this->paginate['conditions'][] = [
                                              'OfOficiosInsumos.cat_insumo_id IN
                                              (
                                                  SELECT
                                                    ci.id
                                                  FROM
                                                    cat_insumos ci
                                                  WHERE
                                                    ci.cat_programa_id = "'.$Filtro['cat_programa_id'].'"
                                              )'
                                            ];
          }
          if(!empty($Filtro['partida']))
          {
            $this->paginate['conditions'][] = [
                                              'OfOficiosInsumos.cat_insumo_id IN
                                              (
                                                  SELECT
                                                    ci.id
                                                  FROM
                                                    cat_insumos ci
                                                  WHERE
                                                    ci.clave_partida = '.$Filtro['partida'].'
                                              )'
                                            ];
          }
          if(!empty($Filtro['oficio']))
          {
            $this->paginate['conditions'][] = [
                                              'OfOficiosInsumos.of_oficio_id IN
                                              (
                                                  SELECT
                                                    of.id
                                                  FROM
                                                    of_oficios of
                                                  WHERE
                                                    of.num_oficio = "'.$Filtro['oficio'].'"
                                              )'
                                            ];
          }
        }

        if(!empty($this->paginate['conditions']))
        {
            $query = $this->OfOficiosInsumos->find()->where($this->paginate['conditions']);

             $this->paginate['contain'] = ['CoUsuarios', 'OfOficios', 'CatInsumos'];
            $ofOficiosInsumos = $this->paginate($query);
        }
        else
        {
        	$this->paginate['contain'] = ['CoUsuarios', 'OfOficios', 'CatInsumos'];

            $ofOficiosInsumos = $this->paginate('OfOficiosInsumos');
        }

        //Numero total de registros
        $iTotalDisplayRecords = $this->request->params['paging']['OfOficiosInsumos']['count'];

        //Numero de registros encontrados
        $iTotalRecords = $this->request->params['paging']['OfOficiosInsumos']['current'];

        //sEcho
        $sEcho = intval($this->request->query['sEcho']);

        //Datos para la tabla
        $aaData = array();

        //Cargamos los Helper para armar los links de acciones
        $View = new \App\View\AppView();
        App::classname('Html', 'View/Helper', 'Helper');
        $Html = $View->loadHelper('Html');
        $Form = $View->loadHelper('Form');

        $i = 0;
        foreach($ofOficiosInsumos as $ofOficiosInsumo)
        {
          if ($ofOficiosInsumo->activo == 0)
          {
            $aaData[$i]['DT_RowClass'] = "table-success";
          }
            $actions = "<div class='btn-group' role='group'>";
                $actions .= $Html->link("<i class='icon md-eye' aria-hidden='true'></i>",array('action'=>'view',$ofOficiosInsumo->id),array('escape'=>false,'class'=>"btn btn-default"));
                $actions .= $Html->link("<i class='icon md-edit' aria-hidden='true'></i>",array('action'=>'edit',$ofOficiosInsumo->id),array('escape'=>false,'class'=>"btn btn-default"));
                $actions .= $Form->postLink("<i class='icon md-delete' aria-hidden='true'></i>", ['action' => 'delete',$ofOficiosInsumo->id], ['escape'=>false,'class'=>"btn btn-danger",'confirm' => __('Realmente desea eliminar el registro con el Id # {0}?', $ofOficiosInsumo->id)]);
            $actions .="</div>";
          		//$aaData[$i][] = $ofOficiosInsumo->has('co_usuario') ? $ofOficiosInsumo->co_usuario->nombre_completo : '';
          		$aaData[$i][] = $ofOficiosInsumo->has('of_oficio')? $ofOficiosInsumo->of_oficio->num_oficio : '';
          		$aaData[$i][] = $ofOficiosInsumo->has('cat_insumo') ? 'PARTIDA: '.$ofOficiosInsumo->cat_insumo->clave_partida.'<br> '.$ofOficiosInsumo->cat_insumo->insumo : '';
              $aaData[$i][] = $ofOficiosInsumo->has('cat_insumo') ? $ofOficiosInsumo->cat_insumo->descripcion_insumo.'<br> <b>CANTIDAD</b>: '.$ofOficiosInsumo->cat_insumo->cantidad.'<br> <b>P.U: </b>'.number_format($ofOficiosInsumo->cat_insumo->precio_unitario,2).'<br> <b>TOTAL</b>: '.number_format($ofOficiosInsumo->cat_insumo->total,2) : '';
          		$aaData[$i][] = (!empty($ofOficiosInsumo->fecha_entrega)) ? date_format($ofOficiosInsumo->fecha_entrega,"d-m-Y") : '';
          		$aaData[$i][] = $ofOficiosInsumo->cantidad;
          		$aaData[$i][] = number_format($ofOficiosInsumo->precio_unitario,2);
              $aaData[$i][] = number_format(($ofOficiosInsumo->cantidad * $ofOficiosInsumo->precio_unitario),2);

          		//$aaData[$i][] = date_format($ofOficiosInsumo->created,"d-m-Y H:i a");
	            $aaData[$i][] = $actions;
            $i++;
        }
        //Enviamos y serializamos en JSON todas la variables requeridas por el jquery.dataTable
        $this->set(compact('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_serialize',array('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_jsonp',true);
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
      if($this->request->is('post'))
      {
        $this->request->session()->write('FiltroInsumosOficios',$this->request->data);
      }
      if($this->request->session()->check('FiltroInsumosOficios'))
      {
        $Filtro = $this->request->session()->read('FiltroInsumosOficios');
        $this->request->data = $Filtro;
      }

      $catProgramas = $this->OfOficiosInsumos->CatInsumos->CatProgramas->find('list');
      $catFuentesFinanciamientos = $this->OfOficiosInsumos->CatInsumos->CatFuentesFinanciamientos->find('list');

      $this->set(compact('catProgramas','catFuentesFinanciamientos'));
    }

    /**
     * View method
     *
     * @param string|null $id Of Oficios Insumo id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ofOficiosInsumo = $this->OfOficiosInsumos->get($id, [
            'contain' => ['CoUsuarios', 'OfOficios', 'CatInsumos']
        ]);

        $this->set('ofOficiosInsumo', $ofOficiosInsumo);
        $this->set('_serialize', ['ofOficiosInsumo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ofOficiosInsumo = $this->OfOficiosInsumos->newEntity();
        if ($this->request->is('post'))
        {
            $ofOficiosInsumo = $this->OfOficiosInsumos->patchEntity($ofOficiosInsumo, $this->request->getData());
            if ($this->OfOficiosInsumos->save($ofOficiosInsumo))
            {
                $this->request->session()->write('data',$this->request->getData());
                $this->Flash->flash('Registro guardado.', ['params'=>['type'=>'info']]);

                return $this->redirect(['action' => 'add']);
            }
			 	$this->Flash->flash('El Registro no pudo ser guardado.', ['params'=>['type'=>'danger']]);

        }
        $coUsuarios = $this->OfOficiosInsumos->CoUsuarios->find('list', ['limit' => 200]);

        if($this->request->session()->check('data'))
        {
          $Filtro = $this->request->session()->read('data');
          $this->request->data = $Filtro;
        }

        if($this->request->session()->check('FiltroInsumosOficios'))
        {
          $Filtro = $this->request->session()->read('FiltroInsumosOficios');

          if(!empty($Filtro['cat_programa_id']))
          {
            $catUnidad = $this->OfOficiosInsumos->CatInsumos->CatProgramas->get($Filtro['cat_programa_id']);

            $catInsumos = $this->OfOficiosInsumos->CatInsumos->find('list', ['conditions'=>['CatInsumos.cat_programa_id'=>$Filtro['cat_programa_id'],'CatInsumos.cat_fuentes_financiamiento_id'=>$Filtro['cat_fuentes_financiamiento_id']]]);
            $ofOficios = $this->OfOficiosInsumos->OfOficios->find('list',['conditions'=>['OfOficios.cat_unidade_id'=>$catUnidad->cat_unidade_id]]);
          }
        }
        else
        {
          $ofOficios = $this->OfOficiosInsumos->OfOficios->find('list');
          $catInsumos = $this->OfOficiosInsumos->CatInsumos->find('list');
        }
        $this->set(compact('ofOficiosInsumo', 'coUsuarios', 'ofOficios', 'catInsumos'));
        $this->set('_serialize', ['ofOficiosInsumo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Of Oficios Insumo id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ofOficiosInsumo = $this->OfOficiosInsumos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ofOficiosInsumo = $this->OfOficiosInsumos->patchEntity($ofOficiosInsumo, $this->request->getData());
            if ($this->OfOficiosInsumos->save($ofOficiosInsumo))
            {
            	$this->Flash->flash('Registro actualizado correctamente.', ['params'=>['type'=>'info']]);
                return $this->redirect(['action' => 'index']);
            }
           		$this->Flash->flash('El registro no se pudo actualizar correctamente. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
    }
        $coUsuarios = $this->OfOficiosInsumos->CoUsuarios->find('list', []);
        $ofOficios = $this->OfOficiosInsumos->OfOficios->find('list', []);
        //$catInsumos = $this->OfOficiosInsumos->CatInsumos->find('list', ['conditions'=>['CatInsumos.cat_fuentes_financiamiento_id'=>'1199048c-92a5-4278-9b5d-39b69d677bfe']]);
        $catInsumos = $this->OfOficiosInsumos->CatInsumos->find('list');

        $this->set(compact('ofOficiosInsumo', 'coUsuarios', 'ofOficios', 'catInsumos'));
        $this->set('_serialize', ['ofOficiosInsumo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Of Oficios Insumo id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ofOficiosInsumo = $this->OfOficiosInsumos->get($id);
        $ofOficiosInsumo->activo = 0;
        if ($this->OfOficiosInsumos->save($ofOficiosInsumo))
        {
        	$this->Flash->flash('Registro eliminado correctamente.', ['params'=>['type'=>'info']]);
        }
        else
        {
			$this->Flash->flash('El registro no pudo ser eliminado. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
        }

        return $this->redirect(['action' => 'index']);
    }

}
