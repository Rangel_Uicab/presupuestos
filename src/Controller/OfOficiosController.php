<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\App;

/**
 * OfOficios Controller
 *
 * @property \App\Model\Table\OfOficiosTable $OfOficios
 *
 * @method \App\Model\Entity\OfOficio[] paginate($object = null, array $settings = [])
 */
class OfOficiosController extends AppController
{
    public $paginate = array();


    public function getData()
    {
         $aColumns = array
                        (
                            'CatUnidades.name',
                            'CatProgramas.name',
                            'OfOficios.num_oficio',
                            'OfOficios.asunto',
                            'OfOficios.resumen'
                            );

        $sIndexColumn = "OfOficios.id";

        //Verificamos que nos enviaron la cantidad de registros que se requieren por pagina
        if(isset($this->request->query['iDisplayLength']))
        {
        	$this->paginate['maxLimit'] =$this->request->query['iDisplayLength'];
        	$this->paginate['limit'] =$this->request->query['iDisplayLength'];
        }

        //Verificamos si nos enviaron la pagina que desean visualizar
        if(isset($this->request->query['iDisplayStart']))
        {
            //Se realiza la division para obtener el numero de pagina
            $this->paginate['page'] = ($this->request->query['iDisplayStart']/$this->request->query['iDisplayLength'])+1;
        }
        //Verificamos si se envio algun orden de columna en especifico
        if(isset($this->request->query['iSortCol_0']))
        {
            $orden = [];
            for ( $i=0 ; $i < intval( $this->request->query['iSortingCols'] ) ; $i++ )
            {
                if ( $this->request->query[ 'bSortable_'.intval($this->request->query['iSortCol_'.$i]) ] == "true" )
                {
                    $column = $aColumns[ intval( $this->request->query['iSortCol_'.$i] ) ];
                	$order = ($this->request->query['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
                	$orden = [ $column => $order];
                }
            }
            //Si la cadena no esta vacia se la agregamos a las opciones del paginador en la opcion "order"
            if ( !empty($orden))
            {
                $this->paginate['order'] = $orden;
            }
        }
        //Revisamos si se envio el filtro para todos los campos
        $conditions = array();
        if ( isset($this->request->query['sSearch']) && $this->request->query['sSearch'] != "" )
        {
            for ( $i=0 ; $i < count($aColumns) ; $i++ )
            {
                $conditions[][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
            }
            //Si el arreglo de condiciones no esta vacio, lo pasamos a las opciones del Paginador con el operador OR
            if(!empty($conditions))
            {
                $this->paginate['conditions']['OR'] = $conditions;
            }
        }
        //Verificamos si se envio algun filtro de campo especifico
        for ( $i=0 ; $i < count($aColumns) ; $i++ )
        {
            if ( isset($this->request->query['bSearchable_'.$i]) && $this->request->query['bSearchable_'.$i] == "true" && ($this->request->query['sSearch_'.$i] != '' || $this->request->query['sSearch'] != '') )
            {
                if(!empty($this->request->query['sSearch']))
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
                else
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch_'.$i].'%';
            }
        }

        if($this->request->session()->check('FiltroOficios'))
        {
          $Filtro = $this->request->session()->read('FiltroOficios');

          if(!empty($Filtro['cat_unidade_id']))
          {
            $this->paginate['conditions'][]['OfOficios.cat_unidade_id'] = $Filtro['cat_unidade_id'];
          }
          if(!empty($Filtro['cat_programa_id']))
          {
            $this->paginate['conditions'][]['OfOficios.cat_programa_id'] = $Filtro['cat_programa_id'];
          }
          if(!empty($Filtro['oficio']))
          {
            $this->paginate['conditions'][]['OfOficios.num_oficio LIKE'] = '%'.$Filtro['oficio'].'%';
          }
        }

        if(!empty($this->paginate['conditions']))
        {
            $query = $this->OfOficios->find()->where($this->paginate['conditions']);

             $this->paginate['contain'] = ['CatUnidades', 'CatProgramas'];
            $ofOficios = $this->paginate($query);
        }
        else
        {
        	$this->paginate['contain'] = ['CatUnidades', 'CatProgramas'];

            $ofOficios = $this->paginate('OfOficios');
        }

        //Numero total de registros
        $iTotalDisplayRecords = $this->request->params['paging']['OfOficios']['count'];

        //Numero de registros encontrados
        $iTotalRecords = $this->request->params['paging']['OfOficios']['current'];

        //sEcho
        $sEcho = intval($this->request->query['sEcho']);

        //Datos para la tabla
        $aaData = array();

        //Cargamos los Helper para armar los links de acciones
        $View = new \App\View\AppView();
        App::classname('Html', 'View/Helper', 'Helper');
        $Html = $View->loadHelper('Html');
        $Form = $View->loadHelper('Form');

        $i = 0;
        foreach($ofOficios as $ofOficio)
        {

            $actions = "<div class='btn-group' role='group'>";
                $actions .= $Html->link("<i class='icon md-eye' aria-hidden='true'></i>",array('action'=>'view',$ofOficio->id),array('escape'=>false,'class'=>"btn btn-default"));
                $actions .= $Html->link("<i class='icon md-edit' aria-hidden='true'></i>",array('action'=>'edit',$ofOficio->id),array('escape'=>false,'class'=>"btn btn-default"));
                $actions .= $Form->postLink("<i class='icon md-delete' aria-hidden='true'></i>", ['action' => 'delete',$ofOficio->id], ['escape'=>false,'class'=>"btn btn-danger",'confirm' => __('Realmente desea eliminar el registro con el Id # {0}?', $ofOficio->id)]);
            $actions .="</div>";
          		$aaData[$i][] = $ofOficio->has('cat_unidade') ? $ofOficio->cat_unidade->name : '';
              $aaData[$i][] = $ofOficio->has('cat_programa') ? $ofOficio->cat_programa->name : '';
          		$aaData[$i][] = $ofOficio->num_oficio;
          		$aaData[$i][] = $ofOficio->asunto;
              $aaData[$i][] = $ofOficio->resumen;
          		$aaData[$i][] = (!empty($ofOficio->fecha)) ? date_format($ofOficio->fecha,"d-m-Y") : '';
          		$aaData[$i][] = (!empty($ofOficio->fecha_recepcion)) ? date_format($ofOficio->fecha_recepcion,"d-m-Y") : '';
          		$aaData[$i][] = (!empty($ofOficio->created)) ? date_format($ofOficio->created,"d-m-Y") : '';
	            $aaData[$i][] = $actions;
            $i++;
        }
        //Enviamos y serializamos en JSON todas la variables requeridas por el jquery.dataTable
        $this->set(compact('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_serialize',array('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_jsonp',true);
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
      if($this->request->is('post'))
      {
        $this->request->session()->write('FiltroOficios',$this->request->data);
      }
      if($this->request->session()->check('FiltroOficios'))
      {
        $Filtro = $this->request->session()->read('FiltroOficios');
        $this->request->data = $Filtro;
      }

      $catProgramas = $this->OfOficios->CatProgramas->find('list');
      $catUnidades = $this->OfOficios->CatUnidades->find('list');

      $this->set(compact('catProgramas','catUnidades'));
    }

    /**
     * View method
     *
     * @param string|null $id Of Oficio id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ofOficio = $this->OfOficios->get($id, [
            'contain' => ['CatUnidades', 'CoUsuarios', 'OfOficiosInsumos']
        ]);

        $this->set('ofOficio', $ofOficio);
        $this->set('_serialize', ['ofOficio']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ofOficio = $this->OfOficios->newEntity();
        if ($this->request->is('post')) {
            $ofOficio = $this->OfOficios->patchEntity($ofOficio, $this->request->getData());
            $ofOficio->cat_tipos_oficio_id = "69ad3c89-169f-4f2f-bad8-607de8b4ff69";
            $ofOficio->co_usuario_id = $this->request->session()->read('Auth.User.id');
            $ofOficio->fecha_recepcion = $ofOficio->fecha;
            // pr($ofOficio);exit;
            if ($this->OfOficios->save($ofOficio))
            {
              $this->request->session()->write('OficiosData',$this->request->data);

                $this->Flash->flash('Registro guardado: '.$ofOficio->num_oficio, ['params'=>['type'=>'info']]);

                return $this->redirect(['action' => 'add']);
            }
			 	$this->Flash->flash('El Registro no pudo ser guardado.', ['params'=>['type'=>'danger']]);

        }
        if($this->request->session()->check('OficiosData'))
        {
          $Filtro = $this->request->session()->read('OficiosData');
          $this->request->data = $Filtro;
        }
        $catUnidades = $this->OfOficios->CatUnidades->find('list', ['limit' => 200]);
        $catProgramas = $this->OfOficios->CatProgramas->find('list', ['limit' => 200]);
        $catTiposOficios = $this->OfOficios->CatTiposOficios->find('list', ['limit' => 200]);
        $this->set(compact('ofOficio', 'catUnidades', 'catTiposOficios','catProgramas'));
        $this->set('_serialize', ['ofOficio']);
    }

    public function addOficioInsumo()
    {
        $ofOficio = $this->OfOficios->newEntity();
        if ($this->request->is('post'))
        {
          $this->request->data['of_oficios_insumos'][0]['co_usuario_id'] = $this->request->session()->read('Auth.User.id');

            $ofOficio = $this->OfOficios->patchEntity($ofOficio, $this->request->getData());
            $ofOficio->cat_tipos_oficio_id = "69ad3c89-169f-4f2f-bad8-607de8b4ff69";
            $ofOficio->co_usuario_id = $this->request->session()->read('Auth.User.id');
            $ofOficio->fecha_recepcion = $ofOficio->fecha;
            // pr($ofOficio);exit;
            if ($this->OfOficios->save($ofOficio))
            {
              $this->request->session()->write('OficiosData',$this->request->data);

                $this->Flash->flash('Registro guardado: '.$ofOficio->num_oficio, ['params'=>['type'=>'info']]);

                return $this->redirect(['action' => 'add']);
            }
			 	$this->Flash->flash('El Registro no pudo ser guardado.', ['params'=>['type'=>'danger']]);

        }
        if($this->request->session()->check('OficiosData'))
        {
          $Filtro = $this->request->session()->read('OficiosData');
          $this->request->data = $Filtro;
        }
        $catUnidades = $this->OfOficios->CatUnidades->find('list', ['limit' => 200]);
        $catProgramas = $this->OfOficios->CatProgramas->find('list', ['limit' => 200]);
        $catTiposOficios = $this->OfOficios->CatTiposOficios->find('list', ['limit' => 200]);
        $this->set(compact('ofOficio', 'catUnidades', 'catTiposOficios','catProgramas'));
        $this->set('_serialize', ['ofOficio']);
    }

    /**

     * Edit method
     *
     * @param string|null $id Of Oficio id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ofOficio = $this->OfOficios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ofOficio = $this->OfOficios->patchEntity($ofOficio, $this->request->getData());
            if ($this->OfOficios->save($ofOficio))
            {
            	$this->Flash->flash('Registro actualizado correctamente.', ['params'=>['type'=>'info']]);
                return $this->redirect(['action' => 'index']);
            }
           		$this->Flash->flash('El registro no se pudo actualizar correctamente. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
    }
        $catUnidades = $this->OfOficios->CatUnidades->find('list', ['limit' => 200]);
        $catProgramas = $this->OfOficios->CatProgramas->find('list', ['limit' => 200]);
        $coUsuarios = $this->OfOficios->CoUsuarios->find('list', ['limit' => 200]);
        $this->set(compact('ofOficio', 'catUnidades', 'catProgramas'));
        $this->set('_serialize', ['ofOficio']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Of Oficio id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ofOficio = $this->OfOficios->get($id);
        if ($this->OfOficios->delete($ofOficio))
        {
        	$this->Flash->flash('Registro eliminado correctamente.', ['params'=>['type'=>'info']]);
        }
        else
        {
			$this->Flash->flash('El registro no pudo ser eliminado. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
        }

        return $this->redirect(['action' => 'index']);
    }
}
