<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\App;
use Cake\ORM\TableRegistry;

/**
 * CatProgramas Controller
 *
 * @property \App\Model\Table\CatProgramasTable $CatProgramas
 *
 * @method \App\Model\Entity\CatPrograma[] paginate($object = null, array $settings = [])
 */
class TableroControlController extends AppController
{
    public $paginate = array();

    public function initialize()
    {
      parent::initialize();
      $this->loadModel('CatFuentesFinanciamientos');
      $this->loadModel('CatProgramas');

    }


    public function index()
    {
      $totalesFuentesFinanciamientos = $this->CatFuentesFinanciamientos->find()
                                                                          ->select
                                                                            (
                                                                              [
                                                                                'CatFuentesFinanciamientos.id',
                                                                                'CatFuentesFinanciamientos.name',
                                                                                'TotalSolicitado'=> '(
                                                                                                        SELECT
                                                                                                          SUM((ofi.cantidad * ofi.precio_unitario))
                                                                                                        FROM
                                                                                                          of_oficios_insumos ofi
                                                                                                        INNER JOIN
                                                                                                          cat_insumos ci3 ON ofi.cat_insumo_id = ci3.id
                                                                                                        WHERE
                                                                                                          ci3.cat_fuentes_financiamiento_id = CatFuentesFinanciamientos.id
                                                                                                      )',
                                                                                  'TotalPresupuesto'=> '(
                                                                                                      		SELECT
                                                                                                      			SUM(ci.total)
                                                                                                      		FROM
                                                                                                      			analisis.cat_insumos ci
                                                                                                      		WHERE
                                                                                                      			ci.cat_fuentes_financiamiento_id = CatFuentesFinanciamientos.id
                                                                                                          AND
                                                                                                            ci.activo = 1
                                                                                                        )'
                                                                              ]
                                                                            )->all();
      $totalesProgramasR12 = $this->CatProgramas->find()
                                                    ->select(
                                                      [
                                                          'CatProgramas.name',
                                                          'TotalInsumos'=>'(
                                                                            SELECT
                                                                              COUNT(*) AS total_insumos
                                                                            FROM
                                                                              cat_insumos ci
                                                                            WHERE
                                                                              ci.cat_fuentes_financiamiento_id = "1199048c-92a5-4278-9b5d-39b69d677bfe"
                                                                            AND
                                                                                    ci.cat_programa_id = CatProgramas.id
                                                                            GROUP BY
                                                                              ci.cat_programa_id
                                                                          )',
                                                        'TotalInsumosSolicitado'=>'(
                                                                                    SELECT
                                                                                      COUNT(*)
                                                                                    FROM
                                                                                      of_oficios_insumos ofi
                                                                                    INNER JOIN cat_insumos ci3 ON ofi.cat_insumo_id = ci3.id
                                                                                        WHERE
                                                                                      ci3.cat_fuentes_financiamiento_id = "1199048c-92a5-4278-9b5d-39b69d677bfe"
                                                                                    AND
                                                                                      ci3.cat_programa_id = CatProgramas.id
                                                                                    GROUP BY ci3.cat_programa_id
                                                                                    )',
                                                        'TotalPresupuestoSolicitado'=> '(
                                                                                        SELECT
                                                                                          SUM(ofi.cantidad * ofi.precio_unitario)
                                                                                        FROM
                                                                                          of_oficios_insumos ofi
                                                                                        INNER JOIN cat_insumos ci3 ON ofi.cat_insumo_id = ci3.id
                                                                                            WHERE
                                                                                          ci3.cat_fuentes_financiamiento_id = "1199048c-92a5-4278-9b5d-39b69d677bfe"
                                                                                        AND
                                                                                          ci3.cat_programa_id = CatProgramas.id
                                                                                        GROUP BY ci3.cat_programa_id
                                                                                        )',
                                                        'TotalPresupuesto'=>'(
                                                                              SELECT
                                                                                SUM(ci.total)
                                                                              FROM
                                                                                cat_insumos ci
                                                                              WHERE
                                                                                ci.cat_fuentes_financiamiento_id = "1199048c-92a5-4278-9b5d-39b69d677bfe"
                                                                              AND
                                                                                      ci.cat_programa_id = CatProgramas.id
                                                                              GROUP BY
                                                                                ci.cat_programa_id
                                                                              )'
                                                      ])
                                                    ->all();
      $totalesProgramasA4 = $this->CatProgramas->find()
                                                ->select(
                                                  [
                                                      'CatProgramas.name',
                                                      'TotalInsumos'=>'(
                                                                        SELECT
                                                                          COUNT(*) AS total_insumos
                                                                        FROM
                                                                          cat_insumos ci
                                                                        WHERE
                                                                          ci.cat_fuentes_financiamiento_id = "31d80b6c-4d2d-4372-8cf9-0c1bb58c5046"
                                                                        AND
                                                                                ci.cat_programa_id = CatProgramas.id
                                                                        GROUP BY
                                                                          ci.cat_programa_id
                                                                      )',
                                                    'TotalInsumosSolicitado'=>'(
                                                                                SELECT
                                                                                  COUNT(*)
                                                                                FROM
                                                                                  of_oficios_insumos ofi
                                                                                INNER JOIN cat_insumos ci3 ON ofi.cat_insumo_id = ci3.id
                                                                                    WHERE
                                                                                  ci3.cat_fuentes_financiamiento_id = "31d80b6c-4d2d-4372-8cf9-0c1bb58c5046"
                                                                                AND
                                                                                  ci3.cat_programa_id = CatProgramas.id
                                                                                GROUP BY ci3.cat_programa_id
                                                                                )',
                                                    'TotalPresupuestoSolicitado'=> '(
                                                                                    SELECT
                                                                                      SUM(ofi.cantidad * ofi.precio_unitario)
                                                                                    FROM
                                                                                      of_oficios_insumos ofi
                                                                                    INNER JOIN cat_insumos ci3 ON ofi.cat_insumo_id = ci3.id
                                                                                        WHERE
                                                                                      ci3.cat_fuentes_financiamiento_id = "31d80b6c-4d2d-4372-8cf9-0c1bb58c5046"
                                                                                    AND
                                                                                      ci3.cat_programa_id = CatProgramas.id
                                                                                    GROUP BY ci3.cat_programa_id
                                                                                    )',
                                                    'TotalPresupuesto'=>'(
                                                                          SELECT
                                                                            SUM(ci.total)
                                                                          FROM
                                                                            cat_insumos ci
                                                                          WHERE
                                                                            ci.cat_fuentes_financiamiento_id = "31d80b6c-4d2d-4372-8cf9-0c1bb58c5046"
                                                                          AND
                                                                                  ci.cat_programa_id = CatProgramas.id
                                                                          GROUP BY
                                                                            ci.cat_programa_id
                                                                          )'
                                                  ])
                                                ->all();
      $totalesProgramasR33 = $this->CatProgramas->find()
                                        ->select(
                                          [
                                              'CatProgramas.name',
                                              'TotalInsumos'=>'(
                                                                SELECT
                                                                  COUNT(*) AS total_insumos
                                                                FROM
                                                                  cat_insumos ci
                                                                WHERE
                                                                  ci.cat_fuentes_financiamiento_id = "32a37825-2114-49f4-8766-af0d8e99b223"
                                                                AND
                                                                        ci.cat_programa_id = CatProgramas.id
                                                                GROUP BY
                                                                  ci.cat_programa_id
                                                              )',
                                            'TotalInsumosSolicitado'=>'(
                                                                        SELECT
                                                                          COUNT(*)
                                                                        FROM
                                                                          of_oficios_insumos ofi
                                                                        INNER JOIN cat_insumos ci3 ON ofi.cat_insumo_id = ci3.id
                                                                            WHERE
                                                                          ci3.cat_fuentes_financiamiento_id = "32a37825-2114-49f4-8766-af0d8e99b223"
                                                                        AND
                                                                          ci3.cat_programa_id = CatProgramas.id
                                                                        GROUP BY ci3.cat_programa_id
                                                                        )',
                                            'TotalPresupuestoSolicitado'=> '(
                                                                            SELECT
                                                                              SUM(ofi.cantidad * ofi.precio_unitario)
                                                                            FROM
                                                                              of_oficios_insumos ofi
                                                                            INNER JOIN cat_insumos ci3 ON ofi.cat_insumo_id = ci3.id
                                                                                WHERE
                                                                              ci3.cat_fuentes_financiamiento_id = "32a37825-2114-49f4-8766-af0d8e99b223"
                                                                            AND
                                                                              ci3.cat_programa_id = CatProgramas.id
                                                                            GROUP BY ci3.cat_programa_id
                                                                            )',
                                            'TotalPresupuesto'=>'(
                                                                  SELECT
                                                                    SUM(ci.total)
                                                                  FROM
                                                                    cat_insumos ci
                                                                  WHERE
                                                                    ci.cat_fuentes_financiamiento_id = "32a37825-2114-49f4-8766-af0d8e99b223"
                                                                  AND
                                                                          ci.cat_programa_id = CatProgramas.id
                                                                  GROUP BY
                                                                    ci.cat_programa_id
                                                                  )'
                                          ])
                                        ->all();
      // pr($totalesProgramasR12->toArray());

      $this->set(compact('totalesFuentesFinanciamientos','totalesProgramasR12','totalesProgramasA4','totalesProgramasR33'));
    }

}
