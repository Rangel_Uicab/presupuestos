<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\App;

/**
 * CatProgramas Controller
 *
 * @property \App\Model\Table\CatProgramasTable $CatProgramas
 *
 * @method \App\Model\Entity\CatPrograma[] paginate($object = null, array $settings = [])
 */
class PresupuestosController extends AppController
{
    public $paginate = array();

    public function initialize()
    {
      parent::initialize();
      $this->loadModel('CatProgramas');
      $this->loadModel('CatInsumos');
      $this->loadModel('OfOficiosInsumos');

    }

    public function getData()
    {
         $aColumns = array
                        (
                          'CatUnidades.name',
                          'CatProgramas.name'
                          );

        $sIndexColumn = "CatProgramas.id";

        //Verificamos que nos enviaron la cantidad de registros que se requieren por pagina
        if(isset($this->request->query['iDisplayLength']))
        {
        	$this->paginate['maxLimit'] =$this->request->query['iDisplayLength'];
        	$this->paginate['limit'] =$this->request->query['iDisplayLength'];
        }

        //Verificamos si nos enviaron la pagina que desean visualizar
        if(isset($this->request->query['iDisplayStart']))
        {
            //Se realiza la division para obtener el numero de pagina
            $this->paginate['page'] = ($this->request->query['iDisplayStart']/$this->request->query['iDisplayLength'])+1;
        }
        //Verificamos si se envio algun orden de columna en especifico
        if(isset($this->request->query['iSortCol_0']))
        {
            $orden = [];
            for ( $i=0 ; $i < intval( $this->request->query['iSortingCols'] ) ; $i++ )
            {
                if ( $this->request->query[ 'bSortable_'.intval($this->request->query['iSortCol_'.$i]) ] == "true" )
                {
                    $column = $aColumns[ intval( $this->request->query['iSortCol_'.$i] ) ];
                	$order = ($this->request->query['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
                	$orden = [ $column => $order];
                }
            }
            //Si la cadena no esta vacia se la agregamos a las opciones del paginador en la opcion "order"
            if ( !empty($orden))
            {
                $this->paginate['order'] = $orden;
            }
        }
        //Revisamos si se envio el filtro para todos los campos
        $conditions = array();
        if ( isset($this->request->query['sSearch']) && $this->request->query['sSearch'] != "" )
        {
            for ( $i=0 ; $i < count($aColumns) ; $i++ )
            {
                $conditions[][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
            }
            //Si el arreglo de condiciones no esta vacio, lo pasamos a las opciones del Paginador con el operador OR
            if(!empty($conditions))
            {
                $this->paginate['conditions']['OR'] = $conditions;
            }
        }
        //Verificamos si se envio algun filtro de campo especifico
        for ( $i=0 ; $i < count($aColumns) ; $i++ )
        {
            if ( isset($this->request->query['bSearchable_'.$i]) && $this->request->query['bSearchable_'.$i] == "true" && ($this->request->query['sSearch_'.$i] != '' || $this->request->query['sSearch'] != '') )
            {
                if(!empty($this->request->query['sSearch']))
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
                else
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch_'.$i].'%';
            }
        }

        if(!empty($this->paginate['conditions']))
        {
            $query = $this->CatProgramas->find()->where($this->paginate['conditions']);

             $this->paginate['contain'] = ['CatUnidades'];
            $catProgramas = $this->paginate($query);
        }
        else
        {
        	$this->paginate['contain'] = ['CatUnidades'];

            $catProgramas = $this->paginate('CatProgramas');
        }

        //Numero total de registros
        $iTotalDisplayRecords = $this->request->params['paging']['CatProgramas']['count'];

        //Numero de registros encontrados
        $iTotalRecords = $this->request->params['paging']['CatProgramas']['current'];

        //sEcho
        $sEcho = intval($this->request->query['sEcho']);

        //Datos para la tabla
        $aaData = array();

        //Cargamos los Helper para armar los links de acciones
        $View = new \App\View\AppView();
        App::classname('Html', 'View/Helper', 'Helper');
        $Html = $View->loadHelper('Html');
        $Form = $View->loadHelper('Form');

        $i = 0;
        foreach($catProgramas as $catPrograma)
        {

            $actions = "<div class='btn-group' role='group'>";
                $actions .= $Html->link("<i class='icon md-shopping-cart-plus' aria-hidden='true'></i> RAMO 12",array('action'=>'view',$catPrograma->id,"1199048c-92a5-4278-9b5d-39b69d677bfe"),array('escape'=>false,'class'=>"btn btn-success"));
                $actions .= $Html->link("<i class='icon md-shopping-cart-plus' aria-hidden='true'></i> ANEXO IV",array('action'=>'view',$catPrograma->id,"31d80b6c-4d2d-4372-8cf9-0c1bb58c5046"),array('escape'=>false,'class'=>"btn btn-info"));
                $actions .= $Html->link("<i class='icon md-shopping-cart-plus' aria-hidden='true'></i> RAMO 33",array('action'=>'view',$catPrograma->id,"32a37825-2114-49f4-8766-af0d8e99b223"),array('escape'=>false,'class'=>"btn btn-warning"));

            $actions .="</div>";
      				$aaData[$i][] = $catPrograma->has('cat_unidade') ? $catPrograma->cat_unidade->name : '';
      				$aaData[$i][] = $catPrograma->name;
	            $aaData[$i][] = $actions;
            $i++;
        }
        //Enviamos y serializamos en JSON todas la variables requeridas por el jquery.dataTable
        $this->set(compact('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_serialize',array('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_jsonp',true);
    }

    public function index()
    {
    }

    public function view($id = null, $financiamiento = null)
    {
        $catFuentesFinanciamientos = $this->CatInsumos->CatFuentesFinanciamientos->get($financiamiento);
        $catPrograma = $this->CatProgramas->get($id, [
                                                        'contain' => [
                                                                      'CatUnidades',
                                                                      'CatInsumos'=>[
                                                                                    'CatEstatus',
                                                                                    'conditions'=>[
                                                                                                  'CatInsumos.cat_fuentes_financiamiento_id'=>$financiamiento,
                                                                                                  'CatInsumos.activo'=>1
                                                                                                ]
                                                                                    ]
                                                                      ]
                                                    ]);
        //pr($catPrograma->toArray());exit;

        $this->set('catPrograma', $catPrograma);
        $this->set('catFuentesFinanciamientos', $catFuentesFinanciamientos);

        $this->set('_serialize', ['catPrograma']);
    }

    public function addOficioInsumo()
    {
        $ofOficiosInsumo = $this->OfOficiosInsumos->newEntity();
        if ($this->request->is('post'))
        {
            $ofOficiosInsumo = $this->OfOficiosInsumos->patchEntity($ofOficiosInsumo, $this->request->getData());
            $ofOficiosInsumo->co_usuario_id = $this->request->session()->read('Auth.User.id');
            if ($this->OfOficiosInsumos->save($ofOficiosInsumo))
            {
                $this->Flash->flash('Registro guardado.', ['params'=>['type'=>'info']]);
            }
            else
            {
                $this->Flash->flash('El Registro no pudo ser guardado.', ['params'=>['type'=>'danger']]);
            }

        }
        return $this->redirect($this->request->referer());
    }

    public function editOficioInsumo()
    {
        $this->autoRender = false ;

        $ofOficiosInsumo = $this->OfOficiosInsumos->get($this->request->data['id']);
        $Data = array();

        if ($this->request->is(['post']))
        {
            $ofOficiosInsumo = $this->OfOficiosInsumos->patchEntity($ofOficiosInsumo, $this->request->getData());
            if ($this->OfOficiosInsumos->save($ofOficiosInsumo))
            {
              $Data['data'] = 1;
            }
            else
            {
              $Data['data'] = 0;
            }
        }
        $this->set(compact('data'));
        $this->set('_serialize',['data']);
    }

    public function deleteOficioInsumo()
    {
        $this->autoRender = false ;

        $this->request->allowMethod(['post', 'delete']);
        $ofOficiosInsumo = $this->OfOficiosInsumos->get($this->request->data['cat_oficio_id']);
        if ($this->OfOficiosInsumos->delete($ofOficiosInsumo))
        {
        }
    }

    public function asignarOficio()
    {
       if ($this->request->is('post'))
         {
              $catPrograma = $this->CatProgramas->get($this->request->data['cat_programa_id']);
              $ofOficios = $this->OfOficiosInsumos->OfOficios->find('list', ['conditions' => ['OfOficios.cat_unidade_id'=>$catPrograma->cat_unidade_id]]);

              $catInsumo = $this->CatInsumos->find()
                                                    ->contain(['OfOficiosInsumos'=>['OfOficios','CoUsuarios']])
                                                    ->where(['CatInsumos.id'=>$this->request->data['cat_insumo_id']])
                                                    ->first();
              $ofOficiosInsumo = $this->OfOficiosInsumos->newEntity();

              $this->set(compact('ofOficiosInsumo','catInsumo','ofOficios','catPrograma'));
              $this->set('_serialize',['ofOficiosInsumo']);
         }

    }

    public function oficiosInsumos()
    {
       if ($this->request->is('post'))
         {
              $catInsumo = $this->CatInsumos->find()
                                                    ->contain(['OfOficiosInsumos'=>['OfOficios'=>['CatTiposOficios'],'CoUsuarios']])
                                                    ->where(['CatInsumos.id'=>$this->request->data['cat_insumo_id']])
                                                    ->first();

              $this->set(compact('catInsumo'));
         }

    }

}
