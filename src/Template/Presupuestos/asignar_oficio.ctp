<?php
$this->loadHelper('Form', ['templates' => 'app_form']);
?>
<style>
span.select2-container {
  z-index:10050;
}
</style>
<div class="modal-content">
  <div class="modal-header">
    <h5 class="modal-title" id="exampleModalTabs">
      PROGRAMA: <?php echo $catPrograma->name;?>
      <br>
      <b>INSUMO: </b> <?php echo $catInsumo->insumo;?> <br>
      DESCRIPCI&Oacute;N: <?php echo $catInsumo->descripcion_insumo;?><br>
      <b>PARTIDA: </b> <?php echo $catInsumo->clave_partida;?> - (<?php echo $catInsumo->nombre_partida;?>)
    </h5>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <table class="table table-bordered table-condensed">
        <thead>
          <tr class="table-success" >
            <th style="color:#ffffff"><?= __('Clave') ?></th>
            <th style="color:#ffffff"><?= __('U.Medida') ?></th>
            <th style="color:#ffffff"><?= __('Cantidad') ?></th>
            <th style="color:#ffffff"><?= __('P.Unitario') ?></th>
            <th style="color:#ffffff"><?= __('Total') ?></th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>
              <b> CLAVE INSUMO: </b><?= h($catInsumo->clave_insumo) ?>
              <?php
              if(!empty($catInsumo->clave_cuadro_basico))
              {
              ?>
                <br>
                <b> CLAVE CUADRO BASICO: </b><?= h($catInsumo->clave_cuadro_basico) ?>
              <?php
              }
              ?>
            </td>

              <td><?= h($catInsumo->unidad_medida) ?></td>
              <td><?= number_format($catInsumo->cantidad) ?></td>
              <td><?= number_format($catInsumo->precio_unitario) ?></td>
              <td><?= number_format($catInsumo->total) ?></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <ul class="nav nav-tabs nav-tabs-line" role="tablist">
    <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleLine1"
        aria-controls="exampleLine1" role="tab">ASIGNAR OFICIO</a></li>
    <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleLine2"
        aria-controls="exampleLine2" role="tab">DESCRIPCI&Oacute;N</a></li>
    <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleLine3"
        aria-controls="exampleLine3" role="tab" onclick="CargarOficios()">OFICIOS</a></li>
  </ul>

  <div class="modal-body">

    <div class="tab-content">
      <div class="tab-pane active" id="exampleLine1" role="tabpanel">
            <?= $this->Form->create($ofOficiosInsumo,['url'=>['action'=>'add_oficio_insumo'],'role'=>'form','onsubmit'=>'return checkSubmit();']) ?>
            <?php
              echo $this->Form->hidden('cat_insumo_id', ['value' => $catInsumo->id]);
            ?>
            <div class="row">
              <div class="col-md-7">
                <?php
                  echo $this->Form->control('of_oficio_id', ['empty'=>true,'options' => $ofOficios,'label'=>['text'=>'Oficio']]);
                ?>
              </div>
              <div class="col-md-5">
                <?php
                $Meses = array(
                		'01'=>'Enero',
                		'02'=>'Febrero',
                		'03'=>'Marzo',
                		'04'=>'Abril',
                		'05'=>'Mayo',
                		'06'=>'Junio',
                		'07'=>'Julio',
                		'08'=>'Agosto',
                		'09'=>'Septiembre',
                		'10'=>'Octubre',
                		'11'=>'Noviembre',
                		'12'=>'Diciembre',
                	);
                ?>
                <label>Fecha Entrega Insumo</label><br/>
                <div class="form-group form-inline">
                  <?php
                    //$this->request->data['fecha_entrega']['year'] = date('Y');
                    //$this->request->data['fecha_entrega']['month'] = date('m');
                    //$this->request->data['fecha_entrega']['day'] = date('d');
                  ?>
                  <?php echo $this->Form->day('fecha_entrega',['label'=>['text'=>'Fecha']]);?>
                  <?php echo $this->Form->month('fecha_entrega',['options'=>$Meses]);?>
                  <?php echo $this->Form->year('fecha_entrega',['maxYear' => date('Y'),'minYear' => date('Y') - 1,'label'=>['text'=>'Año']]);?>
                </div>
              </div>
              <div class="col-md-6">
                <?php
                  $this->request->data['cantidad'] = $catInsumo->cantidad;
                  echo $this->Form->control('cantidad', ['label'=>[]]);
                ?>
              </div>
              <div class="col-md-6">
                <?php
                  $this->request->data['precio_unitario'] = $catInsumo->precio_unitario;
                  echo $this->Form->control('precio_unitario', ['label'=>[]]);
                ?>
              </div>
              <div class="col-md-4"></div>
              <div class="col-md-4">
                <?= $this->Form->button('Guardar',array('type'=>'submit','class'=>'btn btn-success btn-block','id'=>'btnGuardar')) ?>
              </div>
            </div>

            <?= $this->Form->end() ?>


      </div>

      <div class="tab-pane" id="exampleLine2" role="tabpanel">

        <b>TIPO INTERNVENCI&Oacute;N: </b><?php echo $catInsumo->tipo_intervencion?> <br>
        <b>CLAVE INTERNVENCI&Oacute;N: </b><?php echo $catInsumo->clave_intervencion?> <br>
        <b>NOMBRE INTERNVENCI&Oacute;N: </b><?php echo $catInsumo->nombre_intervencion?> <br><br>
        <b>CLAVE ESTRATEGIA: </b><?php echo $catInsumo->clave_estrategia?> <br>
        <b>NOMBRE ESTRATEGIA: </b><?php echo $catInsumo->nombre_estrategia?> <br><br>
        <b>CLAVE LINEA ACCI&Oacute;N: </b><?php echo $catInsumo->clave_linea_accion?> <br>
        <b>NOMBRE LINEA ACCI&Oacute;N: </b><?php echo $catInsumo->nombre_linea_accion?> <br><br>
        <b>CLAVE ACTIVIDAD: </b><?php echo $catInsumo->clave_actividad?> <br>
        <b>NOMBRE ACTIVIDAD: </b><?php echo $catInsumo->nombre_actividad?> <br><br>
        <b>CLAVE ACCI&Oacute;N ESPECIFICA: </b><?php echo $catInsumo->clave_accion_especifica?> <br>
        <b>INDICE ACCI&Oacute;N ESPECIFICA: </b><?php echo $catInsumo->indice_accion_especifica?> <br>
        <b>NOMBRE ACCI&Oacute;N ESPECIFICA: </b><?php echo $catInsumo->nombre_accion_especifica?> <br><br>
        <b>ESTATUS SIAFFASPE: </b><?php echo $catInsumo->estatus?> <br>
      </div>

      <div class="tab-pane" id="exampleLine3" role="tabpanel">

      </div>

    </div>
  </div>

  <div class="modal-footer">
     <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
   </div>
</div>


<script type="text/javascript">
  function CargarOficios()
  {
    $.ajax(
      {
        type:'post',
        async:true,
        url:"<?php echo $this->Url->build(['action'=>'oficios_insumos']) ?>",
        beforeSend:function ()
        {
          //$('#loading').css({'display':'inline'});
        },
        data:
            {
              cat_insumo_id: "<?php echo $catInsumo->id?>"
            }
      }).done(function (html)
      {
        $("#exampleLine3").html(html);
      }).fail(function (error)
      {
        console.log(error);
      }).always(function ()
      {
        //$('#loading').css({'display':'none'});
      })
  }

  /*$('a[data-toggle="tab"]').on('shown.bs.tab', function (e)
  {
      var target = $(e.target).attr("href") // activated tab
      alert(target);
  });
  */
</script>
<script type="text/javascript">
$("#of-oficio-id").select2({placeholder: "SELECCIONAR",allowClear: true});
</script>
