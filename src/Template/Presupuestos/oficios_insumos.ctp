<?php
$this->loadHelper('Form', ['templates' => 'app_form']);
?>
  <?php if ($catInsumo->of_oficios_insumos): ?>
    <table class="table table-bordered table-condensed" style="font-size:12px">
      <thead>
          <tr class="table-info" >
            <th style="color:#ffffff"><?= __('Tipo Oficio') ?></th>
            <th style="color:#ffffff"><?= __('Oficio') ?></th>
            <th style="color:#ffffff"><?= __('F.Oficio') ?></th>
            <th style="color:#ffffff"><?= __('F.Recepcion') ?></th>
            <th style="color:#ffffff"><?= __('F.Entrega') ?></th>
            <th style="color:#ffffff"><?= __('Cantidad') ?></th>
            <th style="color:#ffffff"><?= __('P.Unitario') ?></th>
            <th style="color:#ffffff"><?= __('Total') ?></th>
            <th style="color:#ffffff"><?= __('Usuario') ?></th>
            <th>Acciones</th>
          </tr>
      </thead>
    <?php foreach ($catInsumo->of_oficios_insumos as $oficio): ?>
        <tbody>
          <tr>
            <td><?= h($oficio->of_oficio->cat_tipos_oficio->name) ?></td>
            <td><?= h($oficio->of_oficio->num_oficio) ?></td>
            <td><?= date_format($oficio->of_oficio->fecha,"d-m-Y")?></td>
            <td><?= date_format($oficio->of_oficio->fecha_recepcion,"d-m-Y") ?></td>
            <td><?= (!empty($oficio->fecha_entrega)) ? date_format($oficio->fecha_entrega,"d-m-Y") : 'S/F'?></td>
            <td><?= number_format($oficio->cantidad) ?></td>
            <td><?= number_format($oficio->precio_unitario,2) ?></td>
            <td><?= number_format($oficio->cantidad*$oficio->precio_unitario,2) ?></td>
            <td><?= h($oficio->co_usuario->nombre_completo) ?></td>
            <td>
              <!-- Button trigger modal -->
              <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#ModalOficioInsumo<?php echo $oficio->id?>">
                <i class="icon md-edit">&nbsp;</i>
              </button>
              <?= $this->Form->postLink(__('<i class="icon md-delete">&nbsp;</i>'),'javascript:;',['onClick'=>"DeleteOficio('".$oficio->of_oficio->num_oficio."','".$oficio->id."')",'escape'=>false,'class'=>'btn btn-danger btn-xs'])?>
              <!-- Modal -->
              <div class="modal fade modal-3d-flip-vertical" id="ModalOficioInsumo<?php echo $oficio->id?>" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true" data-keyboard="false">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel"><?= h($oficio->of_oficio->num_oficio) ?></h5>
                    </div>
                    <div class="modal-body">
                        <?php
                        $Meses = array(
                        		'01'=>'Enero',
                        		'02'=>'Febrero',
                        		'03'=>'Marzo',
                        		'04'=>'Abril',
                        		'05'=>'Mayo',
                        		'06'=>'Junio',
                        		'07'=>'Julio',
                        		'08'=>'Agosto',
                        		'09'=>'Septiembre',
                        		'10'=>'Octubre',
                        		'11'=>'Noviembre',
                        		'12'=>'Diciembre',
                        	);
                        ?>
                    		<?= $this->Form->create(null,['id'=>$oficio->id,'role'=>'form']) ?>
                            <?php echo $this->Form->hidden('id', ['value' => $oficio->id]); ?>

                            <label>Fecha Entrega Insumo</label><br/>
                            <div class="form-group form-inline">
                              <?php
                                if(!empty($oficio->fecha_entrega))
                                {
                                  $this->request->data['fecha_entrega']['year'] = date('Y',strtotime($oficio->fecha_entrega));
                                  $this->request->data['fecha_entrega']['month'] = date('m',strtotime($oficio->fecha_entrega));
                                  $this->request->data['fecha_entrega']['day'] = date('d',strtotime($oficio->fecha_entrega));
                                }
                                ?>
                              <?php echo $this->Form->day('fecha_entrega',['label'=>['text'=>'Fecha']]);?>
                              <?php echo $this->Form->month('fecha_entrega',['options'=>$Meses]);?>
                              <?php echo $this->Form->year('fecha_entrega',['maxYear' => date('Y'),'minYear' => date('Y') - 1,'label'=>['text'=>'Año']]);?>
                            </div>

            				        <?php
                            $this->request->data['cantidad'] = $oficio->cantidad;
                            $this->request->data['precio_unitario'] = $oficio->precio_unitario;

                        		echo $this->Form->control('cantidad', ['label'=>[]]);
                        		echo $this->Form->control('precio_unitario', ['label'=>[]]);
            	               ?>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-danger btn-sm" data-number="2">Cancelar</button>
                      <?= $this->Form->button('Actualizar',array('class'=>"btn btn-primary btn-sm",'onClick'=>"EditInsumo('#".$oficio->id."')")) ?>
                      <?= $this->Form->end() ?>
                    </div>
                  </div>
                </div>
              </div>

              <script type="text/javascript">
              $("button[data-number=2]").click(function()
              {
                  $('#ModalOficioInsumo<?php echo $oficio->id?>').modal('hide');
              });
              </script>

            </td>
        </tr>
      </tbody>
    <?php endforeach; ?>
    </table>
  <?php else: ?>
    <div class="alert alert-warning" role="alert">
      SIN OFICIOS ASIGNADOS...
    </div>
  <?php endif; ?>

<script type="text/javascript">
  function DeleteOficio(oficio,id)
  {
      if (confirm("REALMENTE DESEA ELIMINAR EL REGISTRO CON EL OFICIO: "+oficio+"?"))
      {
        $.ajax({
               type: "POST",
               url: "<?php echo $this->Url->build(['action'=>'delete_oficio_insumo']) ?>",
               data:
                   {
                     cat_oficio_id: id
                   },
               success: function(data)
               {
                  console.log(data);
                  CargarOficios();
               }
             });
        return true;
      }
      else
      {
        return false;
      }
  }
</script>

<script type="text/javascript">
function EditInsumo(form)
{
    $.ajax({
           type: "POST",
           url: "<?php echo $this->Url->build(['action'=>'edit_oficio_insumo']) ?>",
           data: $(form).serialize(), // serializes the form's elements.
           success: function(data)
           {
              console.log(data);
              CargarOficios();
           }
         });
}
</script>
