
<script type="text/javascript">
var statSend = false;
function checkSubmit()
{
    if (!statSend)
    {
        statSend = true;
        document.getElementById('btnGuardar').disabled = true;
        return true;
    }
    else
    {
        alert("El formulario ya se esta enviando...");
        return false;
    }
}
</script>
<?php
	echo $this->Html->css('select2/select2.minfd53',['block'=>true]);
	echo $this->Html->script('select2/select2.full.minfd53',['block'=>true]);
?>
<?php
  echo $this->Html->css(
  						array(
                                'datatables/datatables.net-bs4/dataTables.bootstrap4.minfd53.css?v4.0.1',
                                'datatables/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.minfd53.css?v4.0.1',
                                'datatables/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.minfd53.css?v4.0.1',
                                'datatables/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.minfd53.css?v4.0.1',
                                'datatables/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.minfd53.css?v4.0.1',
                                'datatables/datatables.net-select-bs4/dataTables.select.bootstrap4.minfd53.css?v4.0.1',
                                'datatables/datatables.net-responsive-bs4/responsive.bootstrap4.min',
                                'datatables/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.minfd53.css?v4.0.1',
                                'datatables/datatables.net-bs4/datatable.minfd53.css?v4.0.1',
						      )
						);
  echo $this->Html->script(
  							array(
                                    'datatables/jquery.dataTablesfd53.js?v4.0.1',
                                    'datatables/datatables.net-bs4/dataTables.bootstrap4fd53.js?v4.0.1',
                                    'datatables/datatables.net-fixedheader/dataTables.fixedHeader.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-fixedcolumns/dataTables.fixedColumns.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-rowgroup/dataTables.rowGroup.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-scroller/dataTables.scroller.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-responsive/dataTables.responsive.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-responsive-bs4/responsive.bootstrap4.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-buttons/dataTables.buttons.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-buttons/buttons.html5.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-buttons/buttons.flash.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-buttons/jszip.min',
                                    'datatables/datatables.net-buttons/pdfmake.min',
                                    'datatables/datatables.net-buttons/vfs_fonts',
                                    'datatables/datatables.net-buttons/buttons.colVis.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-buttons-bs4/buttons.bootstrap4.minfd53.js?v4.0.1',
							      )
							);
?>
<div class="row">
	<div class="col-sm-4">
		<?= $this->Html->link('<i class="icon md-format-list-bulleted">&nbsp;</i>&nbsp;Regresar', ['action' => 'index'],['class'=>'btn btn-default','escape'=>false]) ?>
	</div>
	<div class="col-sm-8">
		<h2><?= h($catPrograma->name) ?> <small><?= $catPrograma->has('cat_unidade') ?$catPrograma->cat_unidade->name : '' ?></small> </h2>
    <h3><?php echo $catFuentesFinanciamientos->name; ?></h3>
	</div>
</div>
<div class="row">
		<div class="col-sm-12" style="text-align:justify;">
				<?php if (!empty($catPrograma->cat_insumos)): ?>
				<table class="table table-sm table-condensed table-striped" id="TableInsumos">
					<thead class="bg-grey-300">
						<tr class="table-info">
							<th style="color:#000000"><?= __('Partida') ?></th>
							<th style="color:#000000"><?= __('Insumo') ?></th>
							<th style="color:#000000"><?= __('Descripcion') ?></th>
							<th style="color:#000000"><?= __('U.Medida') ?></th>
							<th style="color:#000000"><?= __('Cantidad') ?></th>
							<th style="color:#000000"><?= __('P.Unitario') ?></th>
							<th style="color:#000000"><?= __('Total') ?></th>
              <th style="color:#000000"><?= __('Total Solicitado') ?></th>
							<th style="color:#000000"><?= __('Estatus') ?></th>
              <th style="color:#000000"><?= __('Oficios') ?></th>

							<th style="color:#000000"><?= __('Acciones') ?></th>
						</tr>
						</thead>
						<tbody>
						<?php foreach ($catPrograma->cat_insumos as $catInsumos): ?>

            <?php
            if($catInsumos->total_compra == $catInsumos->total)
            {
              ?>
                <tr class="table-success">
              <?php
            }
            elseif($catInsumos->total_compra > $catInsumos->total)
            {
            ?>
                <tr class="table-danger">
            <?php
            }
            elseif( ($catInsumos->total_compra < $catInsumos->total) && ($catInsumos->total_compra != 0) )
            {
              ?>
                  <tr class="table-warning">
              <?php
            }
            else
            {
            ?>
                <tr>
            <?php
            }
            ?>
                <td><?= h($catInsumos->clave_partida) ?></td>
                <td><?= h($catInsumos->insumo) ?></td>
                <td><?= h($catInsumos->descripcion_insumo) ?></td>
                <td><?= h($catInsumos->unidad_medida) ?></td>
                <td><?= number_format($catInsumos->cantidad) ?></td>
                <td><?= number_format($catInsumos->precio_unitario,2) ?></td>
                <td><?= number_format($catInsumos->total,2) ?></td>
                <td><?= number_format($catInsumos->total_compra,2) ?></td>
                <td><?= '<span class="'.$catInsumos->cat_estatus->clase.'">'.$catInsumos->cat_estatus->name.'</span>' ?></td>
                <td><?= number_format($catInsumos->total_oficios) ?></td>
                <td>
                  <?= $this->Html->link('<i class="icon md-edit">&nbsp;</i>', 'javascript:;',['onClick'=>"AgregarCompra('".$catInsumos->id."')",'class'=>'btn btn-info','escape'=>false]) ?>
                </td>
            </tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<?php endif; ?>
	</div>
</div>

<div class="modal fade modal-3d-flip-vertical" id="ModalInsumo" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false"  data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document" id="contenido">

  </div>
</div>

<script type="text/javascript" charset="utf-8">
		function AgregarCompra(id)
		{
      //alert(id);
      if(id)
      {
        $.ajax(
          {
            type:'post',
            async:true,
            url:"<?php echo $this->Url->build(['action'=>'asignar_oficio']) ?>",
            beforeSend:function ()
            {
              $('#loading').css({'display':'inline'});
            },
            data:
                {
                  cat_insumo_id:id,
                  cat_programa_id: "<?php echo $catPrograma->id?>"
                }
          }).done(function (html)
          {
            $("#contenido").html(html);
          }).fail(function (error)
          {
            console.log(error);
          }).always(function ()
          {
            $('#loading').css({'display':'none'});
          })
      }
      else
      {
        alert('DEBE DE INGRESAR EL INSUMO');
      }
      $("#ModalInsumo").modal()
		}
</script>

<script type="text/javascript" charset="utf-8">
	$.extend(true, $.fn.dataTable.defaults,
    {
    	lengthMenu: [[10, 25, 50, 250, 999999], [10, 25, 50, 250, "Todos"]]
	});
	var tbl = $('#TableInsumos');

    var tabla = $('#TableInsumos').DataTable({
            'responsive': false,
						'dom': '<lBf<t>ip>',
        	"buttons":
        			   	[
									{
										extend: 'excelHtml5',
										title: 'Lista de "catPrograma"',
										customize: function ( xlsx ){}
									}
        			   	],
            "language": {
                        "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                        "sZeroRecords": "Sin registros",
                        "sInfo": "Mostrando registros de _START_ a _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Sin registros",
                        "sInfoFiltered": "",//"(Mostrando _MAX_ registros por pagina)",
                        "oPaginate": {
                            "sFirst": "Inicio",
                            "sLast": " Final",
                            "sNext": ">",
                            "sPrevious": "<"
                        },
                        "sSearch": " ",
                        "sSearchPlaceholder": "Filtrar",
                        "sProcessing":"Cargando"
                    },
                    "aoColumnDefs": [
                                            { "bSortable": false, "aTargets": [9,10] }
                                    ]
        });

</script>
