<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row">
	<div class="col-lg-3">
		<div class="list-group">
			<a class="list-group-item active">Acciones</a>
			<?= $this->Html->link('<i class="icon md-format-list-bulleted">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			<?= $this->Html->link('<i class="icon md-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $catInsumo->id],['class'=>'list-group-item','escape'=>false]) ?>
	        <?= $this->Form->postLink(__('<i class="icon md-delete">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $catInsumo->id],['confirm' => __('Realmente desea eliminar el registro con Id # {0}?', $catInsumo->id),'escape'=>false,'class'=>'list-group-item'])?>
	    </div>
	</div>

	<div class="col-sm-9">
		<div class="panel panel-primary panel-line">
            <div class="panel-heading">
                <h2 class="panel-title">Informaci&oacute;n</h2>
            </div>
            <div class="panel-body">
             	<h2><?= h($catInsumo->insumo) ?></h2>
							<h5><?= $catInsumo->descripcion_insumo; ?></h5>
							<b style="font-weight: bold;">CLAVE PARTIDA: </b> <?= h($catInsumo->clave_partida) ?> <br>
							<b style="font-weight: bold;">NOMBRE PARTIDA : </b><?= h($catInsumo->nombre_partida) ?><br><br>

							<b style="font-weight: bold;">CLAVE : </b> <?= h($catInsumo->clave_insumo) ?> <br>
							<b style="font-weight: bold;">CLAVE CUADRO BASICO : </b><?= h($catInsumo->clave_cuadro_basico) ?><br>
							<b style="font-weight: bold;">UNIDAD MEDIDA : </b><?= h($catInsumo->unidad_medida) ?><br>
							<b style="font-weight: bold;">CANTIDAD : </b><?= h($catInsumo->cantidad) ?><br>
							<b style="font-weight: bold;">PRECIO UNITARIO : </b><?= number_format($catInsumo->precio_unitario) ?><br>
							<b style="font-weight: bold;">TOTAL : </b><?= h($catInsumo->total) ?><br>
							<b style="font-weight: bold;">ESTATUS SIAFFASPE : </b><?= h($catInsumo->estatus) ?>


							<table class="table table-sm table-striped">
										<tr>
						            <th scope="row"><?= __('Id') ?></th>
						            <td><?= h($catInsumo->id) ?></td>
						        </tr>
																<tr>
						            <th scope="row"><?= __('Usuario') ?></th>
						            <td><?= $catInsumo->has('co_usuario') ? $this->Html->link($catInsumo->co_usuario->nombre_completo, ['controller' => 'CoUsuarios', 'action' => 'view', $catInsumo->co_usuario->id]) : '' ?></td>
						        </tr>
																<tr>
						            <th scope="row"><?= __('Fuentes Financiamiento') ?></th>
						            <td><?= $catInsumo->has('cat_fuentes_financiamiento') ? $this->Html->link($catInsumo->cat_fuentes_financiamiento->name, ['controller' => 'CatFuentesFinanciamientos', 'action' => 'view', $catInsumo->cat_fuentes_financiamiento->id]) : '' ?></td>
						        </tr>
																<tr>
						            <th scope="row"><?= __('Programa') ?></th>
						            <td><?= $catInsumo->has('cat_programa') ? $this->Html->link($catInsumo->cat_programa->name, ['controller' => 'CatProgramas', 'action' => 'view', $catInsumo->cat_programa->id]) : '' ?></td>
						        </tr>
																<tr>
						            <th scope="row"><?= __('Estatus') ?></th>
						            <td><?= $catInsumo->has('cat_estatus') ? $this->Html->link($catInsumo->cat_estatus->name, ['controller' => 'CatEstatus', 'action' => 'view', $catInsumo->cat_estatus->id]) : '' ?></td>
						        </tr>

																						        <tr>
						            <th scope="row"><?= __('F. Creaci&oacute;n') ?></th>
						            <td><?= h($catInsumo->created) ?></td>
						        </tr>
										        <tr>
						            <th scope="row"><?= __('Ultima Modificaci&oacute;n') ?></th>
						            <td><?= h($catInsumo->modified) ?></td>
						        </tr>
								</table>
				</div>


			    		<table class="table table-sm table-condensed">
								<tr>
				            <th scope="row">
											<b style="font-weight: bold;">TIPO INTERVENCI&Oacute;N : </b> <?= h($catInsumo->tipo_intervencion) ?> <br>
											<b style="font-weight: bold;">CLAVE INTERVENCI&Oacute;N: </b> <?= $this->Number->format($catInsumo->clave_intervencion) ?><br>
											<b style="font-weight: bold;">NOMBRE INTERVENCI&Oacute;N : </b><?= $catInsumo->nombre_intervencion; ?>
										</th>
				        </tr>
								<tr>
				            <th scope="row">
											<b style="font-weight: bold;">INDICE ACCI&Oacute;N ESPECIFICA : </b> <?= h($catInsumo->indice_accion_especifica) ?> <br>
											<b style="font-weight: bold;">CLAVE ACCI&Oacute;N ESPECIFICA : </b> <?= $this->Number->format($catInsumo->clave_accion_especifica) ?><br>
											<b style="font-weight: bold;">NOMBRE ACCI&Oacute;N ESPECIFICA : </b><?= $this->Text->autoParagraph(h($catInsumo->nombre_accion_especifica)); ?>
										</th>
				        </tr>
								<tr>
										<th scope="row">
											<b style="font-weight: bold;">CLAVE ESTRATEGIA : </b> <?= $this->Number->format($catInsumo->clave_estrategia) ?> <br>
											<b style="font-weight: bold;">NOMBRE ESTRATEGIA : </b><?= $this->Text->autoParagraph(h($catInsumo->nombre_estrategia)); ?>
										</th>
								</tr>
								<tr>
										<th scope="row">
											<b style="font-weight: bold;">CLAVE LINEA ACCI&Oacute;N : </b> <?= $this->Number->format($catInsumo->clave_linea_accion) ?> <br>
											<b style="font-weight: bold;">NOMBRE LINEA ACCI&Oacute;N : </b><?= $this->Text->autoParagraph(h($catInsumo->nombre_linea_accion)); ?>
										</th>
								</tr>
								<tr>
										<th scope="row">
											<b style="font-weight: bold;">CLAVE ACTIVIDAD : </b> <?= $this->Number->format($catInsumo->clave_actividad) ?> <br>
											<b style="font-weight: bold;">NOMBRE ACTIVIDAD : </b><?= $this->Text->autoParagraph(h($catInsumo->nombre_actividad)); ?>
										</th>
								</tr>
								</table>

							<div class="related">
				        <h4><?= __('Oficios') ?></h4>
				        <?php if (!empty($catInsumo->of_oficios_insumos)): ?>
				        <table class="table table-sm table-striped" cellpadding="0" cellspacing="0">
				            <tr class="table-dark">
				                <th scope="col"><?= __('Oficio') ?></th>
				                <th scope="col"><?= __('Fecha Entrega') ?></th>
				                <th scope="col"><?= __('Cantidad') ?></th>
				                <th scope="col"><?= __('Precio Unitario') ?></th>
												<th scope="col"><?= __('Total') ?></th>
				                <th scope="col"><?= __('F.Registro') ?></th>
				                <th scope="col" class="actions"><?= __('Ver') ?></th>
				            </tr>
				            <?php foreach ($catInsumo->of_oficios_insumos as $ofOficiosInsumos): ?>
				            <tr>
				                <td><?= $ofOficiosInsumos->has('of_oficio') ? $ofOficiosInsumos->of_oficio->num_oficio : ''?></td>
				                <td><?= h($ofOficiosInsumos->fecha_entrega) ?></td>
				                <td><?= h($ofOficiosInsumos->cantidad) ?></td>
				                <td><?= number_format($ofOficiosInsumos->precio_unitario,2) ?></td>
												<td><?= number_format($ofOficiosInsumos->cantidad * $ofOficiosInsumos->precio_unitario,2) ?></td>
				                <td><?= h($ofOficiosInsumos->created) ?></td>
				                <td class="actions">
				                    <?= $this->Html->link(__('Ver Oficio'), ['controller' => 'OfOficiosInsumos', 'action' => 'view', $ofOficiosInsumos->id]) ?>
				                </td>
				            </tr>
				            <?php endforeach; ?>
				        </table>
				        <?php endif; ?>
				    </div>
				            </div>
        </div>
	</div>
</div>
