<?php
/**
  * @var \App\View\AppView $this
  */
?>

	<div class="col-sm-12">
     	<h5><?= h($catInsumo->insumo) ?></h5>
			<h5><?= $catInsumo->descripcion_insumo; ?></h5>
      <table class="table table-sm table-striped">
            <tr>
                <th scope="row"><?= __('Id') ?></th>
								<th><?= $this->Html->link($catInsumo->id, ['controller' => 'CatInsumos', 'action' => 'edit', $catInsumo->id],['target'=>'_blank']) ?></th>
            </tr>
                        <tr>
                <th scope="row"><?= __('Fuentes Financiamiento') ?></th>
                <td><?= $catInsumo->has('cat_fuentes_financiamiento') ? $this->Html->link($catInsumo->cat_fuentes_financiamiento->name, ['controller' => 'CatFuentesFinanciamientos', 'action' => 'view', $catInsumo->cat_fuentes_financiamiento->id]) : '' ?></td>
            </tr>
                        <tr>
                <th scope="row"><?= __('Programa') ?></th>
                <td><?= $catInsumo->has('cat_programa') ? $this->Html->link($catInsumo->cat_programa->name, ['controller' => 'CatProgramas', 'action' => 'view', $catInsumo->cat_programa->id]) : '' ?></td>
            </tr>
        </table>
			<b style="font-weight: bold;">CLAVE A.ESPECIFICA: </b> <?= h($catInsumo->indice_accion_especifica) ?> <br>
			<b style="font-weight: bold;">CLAVE PARTIDA: </b> <?= h($catInsumo->clave_partida) ?> <br>
			<b style="font-weight: bold;">NOMBRE PARTIDA : </b><?= h($catInsumo->nombre_partida) ?><br><br>

			<b style="font-weight: bold;">CLAVE : </b> <?= h($catInsumo->clave_insumo) ?> <br>
			<b style="font-weight: bold;">UNIDAD MEDIDA : </b><?= h($catInsumo->unidad_medida) ?><br>
			<b style="font-weight: bold;">CANTIDAD : </b><?= h($catInsumo->cantidad) ?><br>
			<b style="font-weight: bold;">PRECIO UNITARIO : </b><?= number_format($catInsumo->precio_unitario,2) ?><br>
			<b style="font-weight: bold;">TOTAL : </b><?= number_format($catInsumo->total,2) ?><br>
			<div class="related">
        <h4><?= __('Oficios') ?></h4>
        <?php if (!empty($catInsumo->of_oficios_insumos)): ?>
        <table class="table table-sm table-striped" cellpadding="0" cellspacing="0">
            <tr class="table-dark">
                <th scope="col"><?= __('Oficio') ?></th>
                <th scope="col"><?= __('Cantidad') ?></th>
                <th scope="col"><?= __('Precio Unitario') ?></th>
								<th scope="col"><?= __('Total') ?></th>
                <th scope="col" class="actions"><?= __('Ver') ?></th>
            </tr>
						<?php
						$total = 0;
						?>
            <?php foreach ($catInsumo->of_oficios_insumos as $ofOficiosInsumos): ?>
            <tr>
                <td><?= $ofOficiosInsumos->has('of_oficio') ? $ofOficiosInsumos->of_oficio->num_oficio : ''?></td>
                <td><?= h($ofOficiosInsumos->cantidad) ?></td>
                <td><?= number_format($ofOficiosInsumos->precio_unitario,2) ?></td>
								<td>
									<?php
									$total += $ofOficiosInsumos->cantidad * $ofOficiosInsumos->precio_unitario;
									?>
									<?= number_format($ofOficiosInsumos->cantidad * $ofOficiosInsumos->precio_unitario,2) ?>
								</td>
                <td class="actions">
                    <?= $this->Html->link(__('Editar'), ['controller' => 'OfOficiosInsumos', 'action' => 'edit', $ofOficiosInsumos->id],['target'=>'_blank']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
						<tr class="table-dark">
							<th colspan="3">TOTAL</th>
							<th><?php echo number_format($total,2)?></th>
							<th></th>
						</tr>
        </table>
        <?php endif; ?>
    </div>
  </div>
