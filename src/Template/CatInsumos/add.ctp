<?php
/**
  * @var \App\View\AppView $this
  */
?>
<script type="text/javascript">
var statSend = false;
function checkSubmit()
{
    if (!statSend)
    {
        statSend = true;
        document.getElementById('btnGuardar').disabled = true;
        return true;
    }
    else
    {
        alert("El formulario ya se esta enviando...");
        return false;
    }
}
</script>
<?php
$this->loadHelper('Form', ['templates' => 'app_form']);
?>
<?php
	echo $this->Html->css('select2/select2.minfd53',['block'=>true]);
	echo $this->Html->script('select2/select2.full.minfd53',['block'=>true]);
?>
<div class="row">
	<div class="col-lg-2">
			<div class="list-group">
				<a class="list-group-item active">Acciones</a>
				<?= $this->Html->link('<i class="icon md-format-list-bulleted">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
							</div>
	</div>

	<div class="col-lg-10">
    	<div class="panel panel-primary panel-line">
	        <div class="panel-heading">
	            <h2 class="panel-title"> <?= __('Nuevo Insumo') ?></h2>
	        </div>
	        <div class="panel-body">
        		<?= $this->Form->create($catInsumo,['role'=>'form','onsubmit'=>'return checkSubmit();']) ?>
        	  <div class="row">
              <div class="col-md-12">
                <?php  echo $this->Form->control('capturado', ['required'=>false,'label'=>['text'=>['SELECCIONAR SI ESTE INSUMO NO SE ENCUENTRA DENTRO DEL PRESUPUESTO DEL SISTEMA DE SIAFFASPE.']]]); ?>
              </div>
              <div class="col-md-6">
                <?php  echo $this->Form->control('cat_programa_id', ['empty'=>true,'required'=>true,'options' => $catProgramas,'label'=>['text'=>['Programa']]]); ?>
              </div>
              <div class="col-md-6">
                <?php  echo $this->Form->control('cat_fuentes_financiamiento_id', ['empty'=>true,'required'=>true,'options' => $catFuentesFinanciamientos,'label'=>['text'=>'Fuente Financiamiento']]); ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <?php  echo $this->Form->control('insumo', ['rows'=>'1','label'=>[]]) ?>
              </div>
              <div class="col-md-12">
                <?php  echo $this->Form->control('descripcion_insumo', ['rows'=>'1','label'=>['']]); ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2">
                <?php  echo $this->Form->control('clave_partida', ['label'=>[]]) ?>
              </div>
              <div class="col-md-10">
                <?php  echo $this->Form->control('nombre_partida', ['rows'=>'1','label'=>['']]); ?>
              </div>
            </div>

            <div class="row">
              <div class="col-md-3">
                <?php  echo $this->Form->control('unidad_medida', ['label'=>[]]) ?>
              </div>
              <div class="col-md-3">
                <?php  echo $this->Form->control('cantidad', ['label'=>[]]) ?>
              </div>
              <div class="col-md-3">
                <?php  echo $this->Form->control('precio_unitario', ['label'=>['']]); ?>
              </div>
              <div class="col-md-3">
                <?php  echo $this->Form->control('total', ['label'=>['']]); ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <?php  echo $this->Form->control('estatus', ['label'=>['text'=>['Estatus Siaffaspe']]]); ?>
              </div>
              <div class="col-md-6">
                <?php  echo $this->Form->control('cat_estatu_id', ['options' => $catEstatus,'label'=>['text'=>'Estatus']]); ?>
              </div>
            </div>

            <button class="btn btn-info" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
              Agregar Detalles
            </button>
            <div class="collapse" id="collapseExample">
              <div class="card card-body">

                <div class="row">
                  <div class="col-md-6">
                    <?php  echo $this->Form->control('clave_insumo', ['label'=>[]]) ?>
                  </div>
                  <div class="col-md-6">
                    <?php  echo $this->Form->control('clave_cuadro_basico', ['label'=>['']]); ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-2">
                    <?php  echo $this->Form->control('tipo_intervencion', ['label'=>[]]) ?>
                  </div>
                  <div class="col-md-2">
                    <?php  echo $this->Form->control('clave_intervencion', ['label'=>['']]); ?>
                  </div>
                  <div class="col-md-8">
                    <?php  echo $this->Form->control('nombre_intervencion', ['rows'=>'1','label'=>['']]); ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-2">
                    <?php  echo $this->Form->control('clave_estrategia', ['label'=>[]]) ?>
                  </div>
                  <div class="col-md-10">
                    <?php  echo $this->Form->control('nombre_estrategia', ['rows'=>'1','label'=>['']]); ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-2">
                    <?php  echo $this->Form->control('clave_linea_accion', ['label'=>[]]) ?>
                  </div>
                  <div class="col-md-10">
                    <?php  echo $this->Form->control('nombre_linea_accion', ['rows'=>'1','label'=>['']]); ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-2">
                    <?php  echo $this->Form->control('clave_actividad', ['label'=>[]]) ?>
                  </div>
                  <div class="col-md-10">
                    <?php  echo $this->Form->control('nombre_actividad', ['rows'=>'1','label'=>['']]); ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-2">
                    <?php  echo $this->Form->control('clave_accion_especifica', ['label'=>[]]) ?>
                  </div>
                  <div class="col-md-2">
                    <?php  echo $this->Form->control('indice_accion_especifica', ['label'=>[]]) ?>
                  </div>
                  <div class="col-md-8">
                    <?php  echo $this->Form->control('nombre_accion_especifica', ['rows'=>'1','label'=>['']]); ?>
                  </div>
                </div>

              </div>
            </div>

       			<?= $this->Form->button('Guardar Insumo',array('type'=>'submit','class'=>'btn btn-primary waves-effect','id'=>'btnGuardar')) ?>

        		<?= $this->Form->end() ?>
    		</div>
	    </div>
	</div>
</div>

<script type="text/javascript">
	$("#cat-programa-id").select2	({placeholder: "SELECCIONAR",allowClear: true});
</script>
