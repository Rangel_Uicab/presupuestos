<?php
/**
  * @var \App\View\AppView $this
  */
?>
<script type="text/javascript">
var statSend = false;
function checkSubmit()
{
    if (!statSend)
    {
        statSend = true;
        document.getElementById('btnGuardar').disabled = true;
        return true;
    }
    else
    {
        alert("El formulario ya se esta enviando...");
        return false;
    }
}
</script>
<?php
echo $this->Html->css('select2/select2.minfd53',['block'=>true]);
echo $this->Html->script('select2/select2.full.minfd53',['block'=>true]);
?>
<?php
$this->loadHelper('Form', ['templates' => 'app_form']);
?>
<?php
$Meses = array(
		'01'=>'Enero',
		'02'=>'Febrero',
		'03'=>'Marzo',
		'04'=>'Abril',
		'05'=>'Mayo',
		'06'=>'Junio',
		'07'=>'Julio',
		'08'=>'Agosto',
		'09'=>'Septiembre',
		'10'=>'Octubre',
		'11'=>'Noviembre',
		'12'=>'Diciembre',
	);
?>
<div class="row">
	<div class="col-lg-5">
			<div class="list-group">
				<a class="list-group-item active">Acciones</a>
				<?= $this->Html->link('<i class="icon md-format-list-bulleted">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			</div>
      <hr>
      <div class="col-md-12" id="DivInsumo">
        Informaci&oacute;n del Insumo
      </div>
	</div>

	<div class="col-lg-7">
    	<div class="panel panel-primary panel-line">
	        <div class="panel-heading">
	            <h2 class="panel-title"> <?= __('Agregar Oficio de Compra') ?></h2>
	        </div>
	        <div class="panel-body">
        		<?= $this->Form->create($ofOficiosInsumo,['role'=>'form','onsubmit'=>'return checkSubmit();']) ?>

				        <?php
            		echo $this->Form->control('co_usuario_id', ['options' => $coUsuarios,'label'=>['text'=>'Usuario']]);
            		echo $this->Form->control('of_oficio_id', ['empty'=>true,'options' => $ofOficios,'label'=>['text'=>'Oficio']]);
            		echo $this->Form->control('cat_insumo_id', ['empty'=>true,'options' => $catInsumos,'label'=>['text'=>'Insumo']]);
                ?>
                <label>Fecha Entrega Insumo</label><br/>
                <div class="form-group form-inline">
                  <?php echo $this->Form->day('fecha_entrega',['label'=>['text'=>'Fecha Entrega']]);?>
                  <?php echo $this->Form->month('fecha_entrega',['options'=>$Meses]);?>
                  <?php echo $this->Form->year('fecha_entrega',['maxYear' => date('Y'),'minYear' => date('Y') - 1,'label'=>['text'=>'Año']]);?>
                </div>
              <div class="form-group form-inline">
                <?php
            		echo $this->Form->control('cantidad', ['label'=>[]]);
            		echo $this->Form->control('precio_unitario', ['label'=>[]]);
	               ?>
              </div>
       			<?= $this->Form->button('Guardar',array('type'=>'submit','class'=>'btn btn-primary waves-effect','id'=>'btnGuardar')) ?>

        		<?= $this->Form->end() ?>
    		</div>
	    </div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){

  <?php
  if($this->request->session()->check('data'))
  {
    $Filtro = $this->request->session()->read('data');
    ?>
    $.ajax({
      type:'post',
      async:true,
      url:"<?php echo $this->Url->build(['action'=>'get_info_insumo']) ?>",
      beforeSend:function () {
        //$('#loading').css({'display':'inline'});
      },
      data:{id:'<?php echo $Filtro['cat_insumo_id']?>'}
    }).done(function (html) {
      $("#DivInsumo").html(html);
    }).fail(function (error) {
      console.log(error);
    }).always(function () {
    //	$('#loading').css({'display':'none'});
  });
    <?php
  }
  ?>
  });
  $('#cat-insumo-id').on('change',function ()
	{
        var id = $(this).val();
        if(id)
        {
      				$.ajax({
      					type:'post',
      					async:true,
      					url:"<?php echo $this->Url->build(['controller'=>'cat_insumos','action'=>'get_info_insumo']) ?>",
      					beforeSend:function () {
      						//$('#loading').css({'display':'inline'});
      					},
      					data:{id:id}
      				}).done(function (html) {
      					$("#DivInsumo").html(html);
      				}).fail(function (error) {
      					console.log(error);
      				}).always(function () {
      				//	$('#loading').css({'display':'none'});
      				})
        }
		else
		{
			alert('DEBE SELECCIONAR EL INSUMO');
		}

  });
</script>
<script type="text/javascript">
$("#co-usuario-id").select2	({placeholder: "SELECCIONAR",allowClear: true});
$("#of-oficio-id").select2	({placeholder: "SELECCIONAR",allowClear: true});
$("#cat-insumo-id").select2	({placeholder: "SELECCIONAR",allowClear: true});

</script>
