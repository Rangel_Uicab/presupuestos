<?php
/**
  * @var \App\View\AppView $this
  */
?>


<?php
  echo $this->Html->css(
  						array(
                                'datatables/datatables.net-bs4/dataTables.bootstrap4.minfd53.css?v4.0.1',
                                'datatables/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.minfd53.css?v4.0.1',
                                'datatables/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.minfd53.css?v4.0.1',
                                'datatables/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.minfd53.css?v4.0.1',
                                'datatables/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.minfd53.css?v4.0.1',
                                'datatables/datatables.net-select-bs4/dataTables.select.bootstrap4.minfd53.css?v4.0.1',
                                'datatables/datatables.net-responsive-bs4/responsive.bootstrap4.min',
                                'datatables/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.minfd53.css?v4.0.1',
                                'datatables/datatables.net-bs4/datatable.minfd53.css?v4.0.1',
						      )
						);
  echo $this->Html->script(
  							array(
                                    'datatables/jquery.dataTablesfd53.js?v4.0.1',
                                    'datatables/datatables.net-bs4/dataTables.bootstrap4fd53.js?v4.0.1',
                                    'datatables/datatables.net-fixedheader/dataTables.fixedHeader.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-fixedcolumns/dataTables.fixedColumns.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-rowgroup/dataTables.rowGroup.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-scroller/dataTables.scroller.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-responsive/dataTables.responsive.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-responsive-bs4/responsive.bootstrap4.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-buttons/dataTables.buttons.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-buttons/buttons.html5.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-buttons/buttons.flash.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-buttons/jszip.min',
                                    'datatables/datatables.net-buttons/pdfmake.min',
                                    'datatables/datatables.net-buttons/vfs_fonts',
                                    'datatables/datatables.net-buttons/buttons.colVis.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-buttons-bs4/buttons.bootstrap4.minfd53.js?v4.0.1',
							      )
							);
?>
<script type="text/javascript">
var statSend = false;
function checkSubmit()
{
    if (!statSend)
    {
        statSend = true;
        document.getElementById('btnGuardar').disabled = true;
        return true;
    }
    else
    {
        alert("El formulario ya se esta enviando...");
        return false;
    }
}
</script>
<?php
echo $this->Html->css('select2/select2.minfd53',['block'=>true]);
echo $this->Html->script('select2/select2.full.minfd53',['block'=>true]);
?>
<?php
$this->loadHelper('Form', ['templates' => 'app_form']);
?>
<div class="row">
	<div class="col-md-12">
	   <div class="text-center">
	     <h2>Administraci&oacute;n de Oficios Insumos</h2>
	   </div>
	    <div class="text-right">
	        <?php echo $this->Html->link('<i class="icon md-plus" aria-hidden="true"></i>&nbsp; Nuevo Registro' , ['action' => 'add'],['escape'=>false,'class'=>'btn btn-primary waves-effect waves-classic']) ?>
	    </div>
	    <br>
	</div>
</div>

<div class="row">
    <div class="col-md-3">
      <?= $this->Form->create(null,['role'=>'form','onsubmit'=>'return checkSubmit();']) ?>
      <?php
        echo $this->Form->control('cat_fuentes_financiamiento_id', ['empty'=>true,'options' => $catFuentesFinanciamientos,'label'=>['text'=>'F. Financiamiento']]);
      ?>
    </div>
    <div class="col-md-3">
      <?php
        echo $this->Form->control('cat_programa_id', ['empty'=>true,'options' => $catProgramas,'label'=>['text'=>'Programa']]);
      ?>
    </div>
    <div class="col-md-1">
      <?php
        echo $this->Form->control('partida', ['placeholder' => 'PARTIDA','label'=>['text'=>'Partida']]);
      ?>
    </div>
    <div class="col-md-3">
      <?php
        echo $this->Form->control('oficio', ['placeholder' => 'NUMERO DE OFICIO','label'=>['text'=>'Oficio']]);
      ?>
    </div>
    <div class="col-md-2">
      <br>
      <?= $this->Form->button('BUSCAR',array('type'=>'submit','class'=>'btn btn-primary waves-effect','id'=>'btnGuardar')) ?>
      <?= $this->Form->end() ?>
    </div>
  </div>

<div class="row">
  <div class="col-lg-12">
	 	<table id="ofOficiosInsumo" class="table table-condensed table-hover table-striped table-bordered table-sm" style="font-size:12px;">
	        <thead>
	             <tr>
		                <th>Oficio</th>
		                <th>Insumo</th>
                    <th>Descripci&oacute;n</th>
		                <th>F.Entrega</th>
		                <th>Cantidad</th>
		                <th>P.U</th>
		                <th>Total</th>
		                <th>Acciones</th>
	            </tr>
	        </thead>
	        <tbody>
	            <tr>
	                <td colspan="8">Cargando datos...</td>
	            </tr>
	        </tbody>
	    </table>
	</div>
 </div>

 <script type="text/javascript">
 $("#cat-programa-id").select2	({placeholder: "SELECCIONAR",allowClear: true});
 </script>
<script type="text/javascript" charset="utf-8">
	$.extend(true, $.fn.dataTable.defaults,
    {
    	lengthMenu: [[10, 25, 50, 250, 999999], [10, 25, 50, 250, "Todos"]]
	});
	var tbl = $('#ofOficiosInsumo');

    var tabla = $('#ofOficiosInsumo').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            'responsive': false,
            "sAjaxSource": "<?php echo $this->Url->build(array('action'=>'get_data','_ext'=>'json')); ?>",
			     'dom': 'lBfprtip',
        	"buttons":
        			   	[
        			   		{
	                            extend: 'colvis',
	                            text: 'Columnas',
	                        },
							{
								extend: 'excelHtml5',
								title: 'Lista de "ofOficiosInsumo"',
								customize: function ( xlsx ){}
							},
							{

								extend: 'pdfHtml5',
								text: 'PDF',
								orientation: 'landscape',
								download: 'open',
                 				title:  'LISTA DE "ofOficiosInsumo"',
                 				exportOptions: {
//															columns: [ 0, 1, 2, 3,4,5 ]
											   },
								customize: function (doc)
														{

//																doc.pageMargins = [10,10,10,10];
															doc.defaultStyle.fontSize = 9;
															doc.styles.tableHeader.fontSize = 10;
															doc.styles.title.fontSize = 13;
															// Remove spaces around page title
															doc.content[0].text = doc.content[0].text.trim();

															var colCount = new Array();
															$(tbl).find('tbody tr:first-child td').each(function()
															{
																if($(this).attr('colspan'))
																{
																    for(var i=1;i<=$(this).attr('colspan');$i++)
																    {
																        colCount.push('*');
																    }
																}
																else
																{
																    colCount.push('*');
																}
															});
															doc.content[1].table.widths = colCount;

															// Create a footer
															doc['footer']=(function(page, pages)
															{
																return {
																    columns: [
																        'Elaborado por: <?php echo $this->request->Session()->read('Auth.User.nombre_completo')?>',
																        {
																            // This is the right column
																            alignment: 'right',
																            text: ['Página ', { text: page.toString() },  ' de ', { text: pages.toString() }]
																        }
																    ],
																    margin: [10, 0]
																}
															});
															var d = new Date();
															var fecha = d.toLocaleDateString('es-CL');
															var hora = d.toLocaleTimeString('es-CL');
															doc.content.splice(
																	  			1, 0,
      																				{
																					  columns:
																					  [
																						  {
																							   alignment: 'left',
																							   text: 'Fecha: '+fecha+' : '+hora+'     ',
																							  margin: [ 0, 0, 40, 0 ],
																							  bold: true
																						  }
																					  ]
	  																				}
	  																		 );
														}
                            },
        			   	],
            "language": {
                        "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                        "sZeroRecords": "Sin registros",
                        "sInfo": "Mostrando registros de _START_ a _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Sin registros",
                        "sInfoFiltered": "",//"(Mostrando _MAX_ registros por pagina)",
                        "oPaginate": {
                            "sFirst": "Inicio",
                            "sLast": " Final",
                            "sNext": ">",
                            "sPrevious": "<"
                        },
                        "sSearch": " ",
                        "sSearchPlaceholder": "Filtrar",
                        "sProcessing":"Cargando"
                    },
                    "aoColumnDefs": [
                                            { "bSortable": false, "aTargets": [7] }
                                    ]
        });

</script>
