<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row">
	<div class="col-lg-3">
		<div class="list-group">
			<a class="list-group-item active">Acciones</a>
			<?= $this->Html->link('<i class="icon md-format-list-bulleted">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			<?= $this->Html->link('<i class="icon md-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $ofOficiosInsumo->id],['class'=>'list-group-item','escape'=>false]) ?>				
	        <?= $this->Form->postLink(__('<i class="icon md-delete">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $ofOficiosInsumo->id],['confirm' => __('Realmente desea eliminar el registro con Id # {0}?', $ofOficiosInsumo->id),'escape'=>false,'class'=>'list-group-item'])?>
	    </div>	
	</div>

	<div class="col-sm-9">
		<div class="panel panel-primary panel-line">
            <div class="panel-heading">
                <h2 class="panel-title">Informaci&oacute;n</h2>
            </div>
            <div class="panel-body">
             	<h3><?= h($ofOficiosInsumo->id) ?></h3>
			    <table class="table table-condensed">
																        <tr>
				            <th scope="row"><?= __('Id') ?></th>
				            <td><?= h($ofOficiosInsumo->id) ?></td>
				        </tr>
														<tr>
				            <th scope="row"><?= __('Co Usuario') ?></th>
				            <td><?= $ofOficiosInsumo->has('co_usuario') ? $this->Html->link($ofOficiosInsumo->co_usuario->nombre_completo, ['controller' => 'CoUsuarios', 'action' => 'view', $ofOficiosInsumo->co_usuario->id]) : '' ?></td>
				        </tr>
														<tr>
				            <th scope="row"><?= __('Of Oficio') ?></th>
				            <td><?= $ofOficiosInsumo->has('of_oficio') ? $this->Html->link($ofOficiosInsumo->of_oficio->id, ['controller' => 'OfOficios', 'action' => 'view', $ofOficiosInsumo->of_oficio->id]) : '' ?></td>
				        </tr>
														<tr>
				            <th scope="row"><?= __('Cat Insumo') ?></th>
				            <td><?= $ofOficiosInsumo->has('cat_insumo') ? $this->Html->link($ofOficiosInsumo->cat_insumo->id, ['controller' => 'CatInsumos', 'action' => 'view', $ofOficiosInsumo->cat_insumo->id]) : '' ?></td>
				        </tr>
																												        <tr>
				            <th scope="row"><?= __('Cantidad') ?></th>
				            <td><?= $this->Number->format($ofOficiosInsumo->cantidad) ?></td>
				        </tr>
								        <tr>
				            <th scope="row"><?= __('Precio Unitario') ?></th>
				            <td><?= $this->Number->format($ofOficiosInsumo->precio_unitario) ?></td>
				        </tr>
																				        <tr>
				            <th scope="row"><?= __('Fecha Entrega') ?></th>
				            <td><?= h($ofOficiosInsumo->fecha_entrega) ?></td>
				        </tr>
								        <tr>
				            <th scope="row"><?= __('Created') ?></th>
				            <td><?= h($ofOficiosInsumo->created) ?></td>
				        </tr>
								        <tr>
				            <th scope="row"><?= __('Modified') ?></th>
				            <td><?= h($ofOficiosInsumo->modified) ?></td>
				        </tr>
																    </table>
								            </div>
        </div>
	</div>
</div>
