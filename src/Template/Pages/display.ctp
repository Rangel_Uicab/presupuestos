<?php $this->loadHelper('Form', ['templates' => 'app_form']); ?>
<div class="col-md-12" style="text-align:center;">
    <?php
    echo $this->Form->create('CoUsuario',['class'=>'form','role'=>'form']);
        ?>
        <div class="card">
            <div class="card-header">
                <h3>Bienvenido <?php echo $this->request->session()->read('Auth.User.nombre_completo'); ?></h3>
            </div>
            <div class="card-content">
                <h3>Su perfil cuenta con mas un rol de sistema. Por favor seleccione con cual desea trabajar</h3>
                <?php
                echo $this->Form->input('co_grupo_id', ['label'=>false,'empty'=>true,'required'=>'required','options' => $this->request->session()->read('gruposActivos')]);
                ?>
            </div>
            <div class="card-footer">
                <?php echo $this->Form->button('<i class="icon md-sign-in">&nbsp;</i>&nbsp; ENTRAR',['id'=>'btnGuardar','class'=>'btn btn-info','escape'=>false]) ?>
            </div>
        </div>
        <?php
    echo $this->Form->end();
    ?>
</div>
