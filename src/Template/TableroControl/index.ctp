<?php
/**
  * @var \App\View\AppView $this
  */
?>


<?php
  echo $this->Html->css(
  						array(
                                'datatables/datatables.net-bs4/dataTables.bootstrap4.minfd53.css?v4.0.1',
                                'datatables/datatables.net-responsive-bs4/responsive.bootstrap4.min',
                                'datatables/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.minfd53.css?v4.0.1',
                                'datatables/datatables.net-bs4/datatable.minfd53.css?v4.0.1',
						      )
						);
  echo $this->Html->script(
  							array(
                                    'datatables/jquery.dataTablesfd53.js?v4.0.1',
                                    'datatables/datatables.net-bs4/dataTables.bootstrap4fd53.js?v4.0.1',
                                    'datatables/datatables.net-responsive-bs4/responsive.bootstrap4.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-buttons/dataTables.buttons.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-buttons/buttons.html5.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-buttons/buttons.flash.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-buttons/jszip.min',
                                    'datatables/datatables.net-buttons/pdfmake.min',
                                    'datatables/datatables.net-buttons/vfs_fonts',
                                    'datatables/datatables.net-buttons/buttons.colVis.minfd53.js?v4.0.1',
                                    'datatables/datatables.net-buttons-bs4/buttons.bootstrap4.minfd53.js?v4.0.1',
							      )
							);
?>
<?php
    echo $this->Html->script
                            (
                                array
                                    (
                                        'charts/highcharts',
                                        'charts/highcharts-3d',
                                        'charts/exporting',
                                        'charts/data',
                                        'charts/drilldown',
                                    )
                            );
    ?>
<div class="row">
	<div class="col-lg-12">
	   <div class="text-center">
	     <h2>TABLERO DE CONTROL</h2>
	   </div>
	</div>

	<div class="col-lg-12">
	 	<table id="catPrograma" class="table table-bordered table-striped">
	        <thead>
	             <tr class="table-info">
		                <th>FUENTE DE FINANCIAMIENTO</th>
		                <th class="text-center">TOTAL SOLICITADO</th>
		                <th class="text-center">TOTAL POR SOLICITAR</th>
		                <th class="text-center">TOTAL PRESUPUESTO</th>
		                <th class="text-center">PORCENTAJE SOLICITADO</th>
	            </tr>
	        </thead>
	        <tbody>
            <?php
            $tmp =array();
            $GraficaGeneral =array();
            foreach ($totalesFuentesFinanciamientos as $key => $fuente)
            {
              $totalSolicitado = (int)$fuente->TotalSolicitado;
              $totalPorSolicitarX = $fuente->TotalPresupuesto -$fuente->TotalSolicitado;
              $totalPorSolicitar = (int)$totalPorSolicitarX;
              ?>
              <tr>
	                <td><?php echo $fuente->name?></td>
	                <td class="text-center"><?php echo number_format($fuente->TotalSolicitado ,2 )?></td>
	                <td class="text-center"><?php echo number_format($totalPorSolicitarX,2)?></td>
	                <td class="text-center"><?php echo number_format($fuente->TotalPresupuesto,2)?></td>
	                <td class="text-center"><?php echo round($fuente->TotalSolicitado / $fuente->TotalPresupuesto * 100, 2).'%'?></td>
	            </tr>
              <?php
              $tmp['name'] = $fuente->name;
              $tmp['colorByPoint'] = true;
              $tmp['data'][0] = array('name'=>'TOTAL SOLICITADO','y'=>$totalSolicitado);
              $tmp['data'][1] = array('name'=>'TOTAL POR SOLICITAR','y'=>$totalPorSolicitar);
              $GraficaGeneral[]=$tmp;
            }
            ?>

	        </tbody>
	    </table>
	</div>
</div>

 <div class="row">
  <?php
    foreach ($GraficaGeneral as $key => $grafica)
    {
      ?>
      <div class="col-lg-4" id="container<?php echo $key?>" style="height: 300px; auto"></div>
      <script type="text/javascript" charset="utf-8">
      // Build the chart
      Highcharts.chart('container<?php echo $key?>', {
          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
          },
          title: {
              text: '<?php echo $grafica['name']?>'
          },
          tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
          },
          plotOptions: {
              pie: {
                   shadow: false,
                   center: ['50%', '50%'],
                  allowPointSelect: true,
                  dataLabels: {
                                  enabled: true,
                                  format: '{point.percentage:.2f}%'
                              },
                  showInLegend: true
              }
          },
          exporting:
          {
              enabled: false
          },
          series: [<?php echo json_encode($grafica)?>]
      });
      </script>
      <?php
    }
  ?>
 </div>
 <div class="row">
   <div class="col-md-12">
       <ul class="nav nav-tabs nav-tabs-line">
          <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#r12">RAMO 12</a></li>
          <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#an4">ANEXO IV</a></li>
          <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#r33">RAMO 33</a></li>
        </ul>
      <div class="tab-content">
        <div class="tab-pane container active" id="r12">
          <h2>RAMO 12</h2>
            <table id="ramo12" class="table table-bordered table-striped">
      	        <thead>
      	             <tr class="table-info">
      		                <th>PROGRAMA</th>
      		                <th class="text-center">TOTAL DE INSUMOS SOLICITADOS</th>
      		                <th class="text-center">TOTAL DE INSUMOS</th>
      		                <th class="text-center">TOTAL PRESUPUESTO SOLICITADO</th>
      		                <th class="text-center">TOTAL PRESUPUESTO</th>
      		                <th class="text-center">PORCENTAJE SOLICITADO</th>
      	            </tr>
      	        </thead>
      	        <tbody>
                  <?php
                  $tmp =array();
                  $GraficaGeneral =array();

                  $totalInsumosSolicitados = 0;
                  $totalInsumos = 0;
                  $totalInsumosPresupuestoSolicitado = 0;
                  $totalInsumosPresupuesto = 0;

                  foreach ($totalesProgramasR12 as $key => $programa)
                  {
                    $totalInsumosSolicitados += $programa->TotalInsumosSolicitado;
                    $totalInsumos += $programa->TotalInsumos;
                    $totalInsumosPresupuestoSolicitado += $programa->TotalPresupuestoSolicitado;
                    $totalInsumosPresupuesto += $programa->TotalPresupuesto;
                    // $totalSolicitado = (int)$fuente->TotalSolicitado;
                    // $totalPorSolicitarX = $fuente->TotalPresupuesto -$fuente->TotalSolicitado;
                    // $totalPorSolicitar = (int)$totalPorSolicitarX;
                    ?>
                    <tr>
      	                <td><?php echo $programa->name?></td>
      	                <td class="text-center"><?php echo number_format($programa->TotalInsumosSolicitado)?></td>
      	                <td class="text-center"><?php echo number_format($programa->TotalInsumos)?></td>
      	                <td class="text-center"><?php echo number_format($programa->TotalPresupuestoSolicitado,2)?></td>
      	                <td class="text-center"><?php echo number_format($programa->TotalPresupuesto,2)?></td>
      	                <td class="text-center">
                          <?php
                            if($programa->TotalPresupuestoSolicitado == 0 || $programa->TotalPresupuestoSolicitado == "")
                            {
                              echo '0.00%';
                            }
                            else
                            {
                              echo round($programa->TotalPresupuestoSolicitado / $programa->TotalPresupuesto * 100, 2).'%';
                            }
                          ?>
                        </td>
      	            </tr>
                    <?php
                    // $tmp['name'] = $fuente->name;
                    // $tmp['colorByPoint'] = true;
                    // $tmp['data'][0] = array('name'=>'TOTAL SOLICITADO','y'=>$totalSolicitado);
                    // $tmp['data'][1] = array('name'=>'TOTAL POR SOLICITAR','y'=>$totalPorSolicitar);
                    // $GraficaGeneral[]=$tmp;
                  }
                  ?>

      	        </tbody>
                <tfoot>
                    <tr class="table-info">
                        <th>TOTALES</th>
                        <th class="text-center"><?php echo number_format($totalInsumosSolicitados)?></th>
                        <th class="text-center"><?php echo number_format($totalInsumos)?></th>
                        <th class="text-center"><?php echo number_format($totalInsumosPresupuestoSolicitado,2)?></th>
                        <th class="text-center"><?php echo number_format($totalInsumosPresupuesto,2)?></th>
                        <th class="text-center">
                          <?php
                            if($totalInsumosPresupuestoSolicitado == 0 || $totalInsumosPresupuestoSolicitado == "")
                            {
                              echo '0.00%';
                            }
                            else
                            {
                              echo round($totalInsumosPresupuestoSolicitado / $totalInsumosPresupuesto * 100, 2).'%';
                            }
                          ?>
                        </th>
                    </tr>
                </tfoot>
      	    </table>
        </div>
        <div class="tab-pane container fade" id="an4">
          <h2>ANEXO IV</h2>
          <table id="anexo4" class="table table-bordered table-striped">
              <thead>
                   <tr class="table-info">
                        <th>PROGRAMA</th>
                        <th class="text-center">TOTAL DE INSUMOS SOLICITADOS</th>
                        <th class="text-center">TOTAL DE INSUMOS</th>
                        <th class="text-center">TOTAL PRESUPUESTO SOLICITADO</th>
                        <th class="text-center">TOTAL PRESUPUESTO</th>
                        <th class="text-center">PORCENTAJE SOLICITADO</th>
                  </tr>
              </thead>
              <tbody>
                <?php
                $tmp =array();
                $GraficaGeneral =array();

                $totalInsumosSolicitados = 0;
                $totalInsumos = 0;
                $totalInsumosPresupuestoSolicitado = 0;
                $totalInsumosPresupuesto = 0;

                foreach ($totalesProgramasA4 as $key => $programa)
                {
                  $totalInsumosSolicitados += $programa->TotalInsumosSolicitado;
                  $totalInsumos += $programa->TotalInsumos;
                  $totalInsumosPresupuestoSolicitado += $programa->TotalPresupuestoSolicitado;
                  $totalInsumosPresupuesto += $programa->TotalPresupuesto;
                  // $totalSolicitado = (int)$fuente->TotalSolicitado;
                  // $totalPorSolicitarX = $fuente->TotalPresupuesto -$fuente->TotalSolicitado;
                  // $totalPorSolicitar = (int)$totalPorSolicitarX;
                  ?>
                  <tr>
                      <td><?php echo $programa->name?></td>
                      <td class="text-center"><?php echo number_format($programa->TotalInsumosSolicitado)?></td>
                      <td class="text-center"><?php echo number_format($programa->TotalInsumos)?></td>
                      <td class="text-center"><?php echo number_format($programa->TotalPresupuestoSolicitado,2)?></td>
                      <td class="text-center"><?php echo number_format($programa->TotalPresupuesto,2)?></td>
                      <td class="text-center">
                        <?php
                          if($programa->TotalPresupuestoSolicitado == 0 || $programa->TotalPresupuestoSolicitado == "")
                          {
                            echo '0.00%';
                          }
                          else
                          {
                            echo round($programa->TotalPresupuestoSolicitado / $programa->TotalPresupuesto * 100, 2).'%';
                          }
                        ?>
                      </td>
                  </tr>
                  <?php
                  // $tmp['name'] = $fuente->name;
                  // $tmp['colorByPoint'] = true;
                  // $tmp['data'][0] = array('name'=>'TOTAL SOLICITADO','y'=>$totalSolicitado);
                  // $tmp['data'][1] = array('name'=>'TOTAL POR SOLICITAR','y'=>$totalPorSolicitar);
                  // $GraficaGeneral[]=$tmp;
                }
                ?>

              </tbody>
              <tfoot>
                  <tr class="table-info">
                      <th>TOTALES</th>
                      <th class="text-center"><?php echo number_format($totalInsumosSolicitados)?></th>
                      <th class="text-center"><?php echo number_format($totalInsumos)?></th>
                      <th class="text-center"><?php echo number_format($totalInsumosPresupuestoSolicitado,2)?></th>
                      <th class="text-center"><?php echo number_format($totalInsumosPresupuesto,2)?></th>
                      <th class="text-center">
                        <?php
                          if($totalInsumosPresupuestoSolicitado == 0 || $totalInsumosPresupuestoSolicitado == "")
                          {
                            echo '0.00%';
                          }
                          else
                          {
                            echo round($totalInsumosPresupuestoSolicitado / $totalInsumosPresupuesto * 100, 2).'%';
                          }
                        ?>
                      </th>
                  </tr>
              </tfoot>
          </table>
        </div>
        <div class="tab-pane container fade" id="r33">
          <h2>RAMO 33</h2>
          <table id="ramo33" class="table table-bordered table-striped">
              <thead>
                   <tr class="table-info">
                        <th>PROGRAMA</th>
                        <th class="text-center">TOTAL DE INSUMOS SOLICITADOS</th>
                        <th class="text-center">TOTAL DE INSUMOS</th>
                        <th class="text-center">TOTAL PRESUPUESTO SOLICITADO</th>
                        <th class="text-center">TOTAL PRESUPUESTO</th>
                        <th class="text-center">PORCENTAJE SOLICITADO</th>
                  </tr>
              </thead>
              <tbody>
                <?php
                $tmp =array();
                $GraficaGeneral =array();

                $totalInsumosSolicitados = 0;
                $totalInsumos = 0;
                $totalInsumosPresupuestoSolicitado = 0;
                $totalInsumosPresupuesto = 0;

                foreach ($totalesProgramasR33 as $key => $programa)
                {
                  $totalInsumosSolicitados += $programa->TotalInsumosSolicitado;
                  $totalInsumos += $programa->TotalInsumos;
                  $totalInsumosPresupuestoSolicitado += $programa->TotalPresupuestoSolicitado;
                  $totalInsumosPresupuesto += $programa->TotalPresupuesto;
                  // $totalSolicitado = (int)$fuente->TotalSolicitado;
                  // $totalPorSolicitarX = $fuente->TotalPresupuesto -$fuente->TotalSolicitado;
                  // $totalPorSolicitar = (int)$totalPorSolicitarX;
                  ?>
                  <tr>
                      <td><?php echo $programa->name?></td>
                      <td class="text-center"><?php echo number_format($programa->TotalInsumosSolicitado)?></td>
                      <td class="text-center"><?php echo number_format($programa->TotalInsumos)?></td>
                      <td class="text-center"><?php echo number_format($programa->TotalPresupuestoSolicitado,2)?></td>
                      <td class="text-center"><?php echo number_format($programa->TotalPresupuesto,2)?></td>
                      <td class="text-center">
                        <?php
                          if($programa->TotalPresupuestoSolicitado == 0 || $programa->TotalPresupuestoSolicitado == "")
                          {
                            echo '0.00%';
                          }
                          else
                          {
                            echo round($programa->TotalPresupuestoSolicitado / $programa->TotalPresupuesto * 100, 2).'%';
                          }
                        ?>
                      </td>
                  </tr>
                  <?php
                  // $tmp['name'] = $fuente->name;
                  // $tmp['colorByPoint'] = true;
                  // $tmp['data'][0] = array('name'=>'TOTAL SOLICITADO','y'=>$totalSolicitado);
                  // $tmp['data'][1] = array('name'=>'TOTAL POR SOLICITAR','y'=>$totalPorSolicitar);
                  // $GraficaGeneral[]=$tmp;
                }
                ?>

              </tbody>
              <tfoot>
                  <tr class="table-info">
                      <th>TOTALES</th>
                      <th class="text-center"><?php echo number_format($totalInsumosSolicitados)?></th>
                      <th class="text-center"><?php echo number_format($totalInsumos)?></th>
                      <th class="text-center"><?php echo number_format($totalInsumosPresupuestoSolicitado,2)?></th>
                      <th class="text-center"><?php echo number_format($totalInsumosPresupuesto,2)?></th>
                      <th class="text-center">
                        <?php
                          if($totalInsumosPresupuestoSolicitado == 0 || $totalInsumosPresupuestoSolicitado == "")
                          {
                            echo '0.00%';
                          }
                          else
                          {
                            echo round($totalInsumosPresupuestoSolicitado / $totalInsumosPresupuesto * 100, 2).'%';
                          }
                        ?>
                      </th>
                  </tr>
              </tfoot>
          </table>
        </div>
      </div>
  </div>
</div>

<script type="text/javascript" charset="utf-8">
	$.extend(true, $.fn.dataTable.defaults,
    {
    	lengthMenu: [[10, 25, 50, 250, 999999], [10, 25, 50, 250, "Todos"]]
	});
	var tbl = $('#ramo12');

    var tabla = $('#ramo12').DataTable({
            'responsive': true,
			      'dom': 'lBfrtip',
        	   "buttons":
        			   	[
        			   		{
	                            extend: 'colvis',
	                            text: 'Columnas',
	                        },
							{
								extend: 'excelHtml5',
                title:  'AVANCE DE SOLICITUDES RAMO 12',
								customize: function ( xlsx ){}
							},
							{

								extend: 'pdfHtml5',
								text: 'PDF',
								orientation: 'landscape',
								download: 'open',
                 				title:  'AVANCE DE SOLICITUDES RAMO 12',
                 				exportOptions: {
//															columns: [ 0, 1, 2, 3,4,5 ]
											   },
								customize: function (doc)
														{

//																doc.pageMargins = [10,10,10,10];
															doc.defaultStyle.fontSize = 9;
															doc.styles.tableHeader.fontSize = 10;
															doc.styles.title.fontSize = 13;
															// Remove spaces around page title
															doc.content[0].text = doc.content[0].text.trim();

															var colCount = new Array();
															$(tbl).find('tbody tr:first-child td').each(function()
															{
																if($(this).attr('colspan'))
																{
																    for(var i=1;i<=$(this).attr('colspan');$i++)
																    {
																        colCount.push('*');
																    }
																}
																else
																{
																    colCount.push('*');
																}
															});
															doc.content[1].table.widths = colCount;

															// Create a footer
															doc['footer']=(function(page, pages)
															{
																return {
																    columns: [
																        'FUENTE DE FINANCIAMIENTO RAMO 12',
																        {
																            // This is the right column
																            alignment: 'right',
																            text: ['Página ', { text: page.toString() },  ' de ', { text: pages.toString() }]
																        }
																    ],
																    margin: [10, 0]
																}
															});
															var d = new Date();
															var fecha = d.toLocaleDateString('es-CL');
															var hora = d.toLocaleTimeString('es-CL');
															doc.content.splice(
																	  			1, 0,
      																				{
																					  columns:
																					  [
																						  {
																							   alignment: 'left',
																							   text: 'Fecha: '+fecha+' : '+hora+'     ',
																							  margin: [ 0, 0, 40, 0 ],
																							  bold: true
																						  }
																					  ]
	  																				}
	  																		 );
														}
                            },
        			   	],
            "language": {
                        "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                        "sZeroRecords": "Sin registros",
                        "sInfo": "Mostrando registros de _START_ a _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Sin registros",
                        "sInfoFiltered": "",//"(Mostrando _MAX_ registros por pagina)",
                        "oPaginate": {
                            "sFirst": "Inicio",
                            "sLast": " Final",
                            "sNext": ">",
                            "sPrevious": "<"
                        },
                        "sSearch": " ",
                        "sSearchPlaceholder": "Filtrar",
                        "sProcessing":"Cargando"
                    },
                    "aoColumnDefs": [
                                            {
                                              // "bSortable": false, "aTargets": [4]
                                            }
                                    ]
        });

        $.extend(true, $.fn.dataTable.defaults,
          {
          	lengthMenu: [[10, 25, 50, 250, 999999], [10, 25, 50, 250, "Todos"]]
      	});
      	var tbl = $('#anexo4');

          var tabla = $('#anexo4').DataTable({
                  'responsive': true,
      			      'dom': 'lBfrtip',
              	   "buttons":
              			   	[
              			   		{
      	                            extend: 'colvis',
      	                            text: 'Columnas',
      	                        },
      							{
      								extend: 'excelHtml5',
                      title:  'AVANCE DE SOLICITUDES ANEXO IV',
      								customize: function ( xlsx ){}
      							},
      							{

      								extend: 'pdfHtml5',
      								text: 'PDF',
      								orientation: 'landscape',
      								download: 'open',
                       				title:  'AVANCE DE SOLICITUDES ANEXO IV',
                       				exportOptions: {
      //															columns: [ 0, 1, 2, 3,4,5 ]
      											   },
      								customize: function (doc)
      														{

      //																doc.pageMargins = [10,10,10,10];
      															doc.defaultStyle.fontSize = 9;
      															doc.styles.tableHeader.fontSize = 10;
      															doc.styles.title.fontSize = 13;
      															// Remove spaces around page title
      															doc.content[0].text = doc.content[0].text.trim();

      															var colCount = new Array();
      															$(tbl).find('tbody tr:first-child td').each(function()
      															{
      																if($(this).attr('colspan'))
      																{
      																    for(var i=1;i<=$(this).attr('colspan');$i++)
      																    {
      																        colCount.push('*');
      																    }
      																}
      																else
      																{
      																    colCount.push('*');
      																}
      															});
      															doc.content[1].table.widths = colCount;

      															// Create a footer
      															doc['footer']=(function(page, pages)
      															{
      																return {
      																    columns: [
      																        'FUENTE DE FINANCIAMIENTO ANEXO IV',
      																        {
      																            // This is the right column
      																            alignment: 'right',
      																            text: ['Página ', { text: page.toString() },  ' de ', { text: pages.toString() }]
      																        }
      																    ],
      																    margin: [10, 0]
      																}
      															});
      															var d = new Date();
      															var fecha = d.toLocaleDateString('es-CL');
      															var hora = d.toLocaleTimeString('es-CL');
      															doc.content.splice(
      																	  			1, 0,
            																				{
      																					  columns:
      																					  [
      																						  {
      																							   alignment: 'left',
      																							   text: 'Fecha: '+fecha+' : '+hora+'     ',
      																							  margin: [ 0, 0, 40, 0 ],
      																							  bold: true
      																						  }
      																					  ]
      	  																				}
      	  																		 );
      														}
                                  },
              			   	],
                  "language": {
                              "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                              "sZeroRecords": "Sin registros",
                              "sInfo": "Mostrando registros de _START_ a _END_ de un total de _TOTAL_ registros",
                              "sInfoEmpty": "Sin registros",
                              "sInfoFiltered": "",//"(Mostrando _MAX_ registros por pagina)",
                              "oPaginate": {
                                  "sFirst": "Inicio",
                                  "sLast": " Final",
                                  "sNext": ">",
                                  "sPrevious": "<"
                              },
                              "sSearch": " ",
                              "sSearchPlaceholder": "Filtrar",
                              "sProcessing":"Cargando"
                          },
                          "aoColumnDefs": [
                                                  {
                                                    // "bSortable": false, "aTargets": [4]
                                                  }
                                          ]
              });
              $.extend(true, $.fn.dataTable.defaults,
                {
                	lengthMenu: [[10, 25, 50, 250, 999999], [10, 25, 50, 250, "Todos"]]
            	});
            	var tbl = $('#ramo33');

                var tabla = $('#ramo33').DataTable({
                        'responsive': true,
            			      'dom': 'lBfrtip',
                    	   "buttons":
                    			   	[
                    			   		{
            	                            extend: 'colvis',
            	                            text: 'Columnas',
            	                        },
            							{
            								extend: 'excelHtml5',
                            title:  'AVANCE DE SOLICITUDES RAMO 33',
            								customize: function ( xlsx ){}
            							},
            							{

            								extend: 'pdfHtml5',
            								text: 'PDF',
            								orientation: 'landscape',
            								download: 'open',
                             				title:  'AVANCE DE SOLICITUDES RAMO 33',
                             				exportOptions: {
            //															columns: [ 0, 1, 2, 3,4,5 ]
            											   },
            								customize: function (doc)
            														{

            //																doc.pageMargins = [10,10,10,10];
            															doc.defaultStyle.fontSize = 9;
            															doc.styles.tableHeader.fontSize = 10;
            															doc.styles.title.fontSize = 13;
            															// Remove spaces around page title
            															doc.content[0].text = doc.content[0].text.trim();

            															var colCount = new Array();
            															$(tbl).find('tbody tr:first-child td').each(function()
            															{
            																if($(this).attr('colspan'))
            																{
            																    for(var i=1;i<=$(this).attr('colspan');$i++)
            																    {
            																        colCount.push('*');
            																    }
            																}
            																else
            																{
            																    colCount.push('*');
            																}
            															});
            															doc.content[1].table.widths = colCount;

            															// Create a footer
            															doc['footer']=(function(page, pages)
            															{
            																return {
            																    columns: [
            																        'FUENTE DE FINANCIAMIENTO RAMO 33',
            																        {
            																            // This is the right column
            																            alignment: 'right',
            																            text: ['Página ', { text: page.toString() },  ' de ', { text: pages.toString() }]
            																        }
            																    ],
            																    margin: [10, 0]
            																}
            															});
            															var d = new Date();
            															var fecha = d.toLocaleDateString('es-CL');
            															var hora = d.toLocaleTimeString('es-CL');
            															doc.content.splice(
            																	  			1, 0,
                  																				{
            																					  columns:
            																					  [
            																						  {
            																							   alignment: 'left',
            																							   text: 'Fecha: '+fecha+' : '+hora+'     ',
            																							  margin: [ 0, 0, 40, 0 ],
            																							  bold: true
            																						  }
            																					  ]
            	  																				}
            	  																		 );
            														}
                                        },
                    			   	],
                        "language": {
                                    "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                                    "sZeroRecords": "Sin registros",
                                    "sInfo": "Mostrando registros de _START_ a _END_ de un total de _TOTAL_ registros",
                                    "sInfoEmpty": "Sin registros",
                                    "sInfoFiltered": "",//"(Mostrando _MAX_ registros por pagina)",
                                    "oPaginate": {
                                        "sFirst": "Inicio",
                                        "sLast": " Final",
                                        "sNext": ">",
                                        "sPrevious": "<"
                                    },
                                    "sSearch": " ",
                                    "sSearchPlaceholder": "Filtrar",
                                    "sProcessing":"Cargando"
                                },
                                "aoColumnDefs": [
                                                        {
                                                          // "bSortable": false, "aTargets": [4]
                                                        }
                                                ]
                    });
</script>
