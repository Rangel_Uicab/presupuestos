<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row">
	<div class="col-lg-3">
		<div class="list-group">
			<a class="list-group-item active">Acciones</a>
			<?= $this->Html->link('<i class="icon md-format-list-bulleted">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			<?= $this->Html->link('<i class="icon md-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $catFuentesFinanciamiento->id],['class'=>'list-group-item','escape'=>false]) ?>				
	        <?= $this->Form->postLink(__('<i class="icon md-delete">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $catFuentesFinanciamiento->id],['confirm' => __('Realmente desea eliminar el registro con Id # {0}?', $catFuentesFinanciamiento->id),'escape'=>false,'class'=>'list-group-item'])?>
	    </div>	
	</div>

	<div class="col-sm-9">
		<div class="panel panel-primary panel-line">
            <div class="panel-heading">
                <h2 class="panel-title">Informaci&oacute;n</h2>
            </div>
            <div class="panel-body">
             	<h3><?= h($catFuentesFinanciamiento->name) ?></h3>
			    <table class="table table-condensed">
																        <tr>
				            <th scope="row"><?= __('Id') ?></th>
				            <td><?= h($catFuentesFinanciamiento->id) ?></td>
				        </tr>
																        <tr>
				            <th scope="row"><?= __('Name') ?></th>
				            <td><?= h($catFuentesFinanciamiento->name) ?></td>
				        </tr>
																																        <tr>
				            <th scope="row"><?= __('Created') ?></th>
				            <td><?= h($catFuentesFinanciamiento->created) ?></td>
				        </tr>
								        <tr>
				            <th scope="row"><?= __('Modified') ?></th>
				            <td><?= h($catFuentesFinanciamiento->modified) ?></td>
				        </tr>
																				        <tr>
				            <th scope="row"><?= __('Activo') ?></th>
				            <td><?= $catFuentesFinanciamiento->activo ? '<span class="label label-success">SI</span>' : '<span class="label label-danger">NO</span>'; ?></td>
				        </tr>
												    </table>
												    <div class="related">
				        <h4><?= __('Insumos') ?></h4>
				        <?php if (!empty($catFuentesFinanciamiento->cat_insumos)): ?>
				        <table class="table table-bordered" cellpadding="0" cellspacing="0">
				            <tr>
								                <th scope="col"><?= __('Id') ?></th>
								                <th scope="col"><?= __('Co Usuario Id') ?></th>
								                <th scope="col"><?= __('Cat Fuentes Financiamiento Id') ?></th>
								                <th scope="col"><?= __('Cat Programa Id') ?></th>
								                <th scope="col"><?= __('Tipo Intervencion') ?></th>
								                <th scope="col"><?= __('Clave Intervencion') ?></th>
								                <th scope="col"><?= __('Nombre Intervencion') ?></th>
								                <th scope="col"><?= __('Clave Estrategia') ?></th>
								                <th scope="col"><?= __('Nombre Estrategia') ?></th>
								                <th scope="col"><?= __('Clave Linea Accion') ?></th>
								                <th scope="col"><?= __('Nombre Linea Accion') ?></th>
								                <th scope="col"><?= __('Clave Actividad') ?></th>
								                <th scope="col"><?= __('Nombre Actividad') ?></th>
								                <th scope="col"><?= __('Clave Accion Especifica') ?></th>
								                <th scope="col"><?= __('Indice Accion Especifica') ?></th>
								                <th scope="col"><?= __('Nombre Accion Especifica') ?></th>
								                <th scope="col"><?= __('Clave Partida') ?></th>
								                <th scope="col"><?= __('Nombre Partida') ?></th>
								                <th scope="col"><?= __('Clave Insumo') ?></th>
								                <th scope="col"><?= __('Insumo') ?></th>
								                <th scope="col"><?= __('Unidad Medida') ?></th>
								                <th scope="col"><?= __('Descripcion Insumo') ?></th>
								                <th scope="col"><?= __('Cantidad') ?></th>
								                <th scope="col"><?= __('Precio Unitario') ?></th>
								                <th scope="col"><?= __('Total') ?></th>
								                <th scope="col"><?= __('Estatus') ?></th>
								                <th scope="col"><?= __('Cat Estatu Id') ?></th>
								                <th scope="col"><?= __('Created') ?></th>
								                <th scope="col"><?= __('Modified') ?></th>
								                <th scope="col" class="actions"><?= __('Actions') ?></th>
				            </tr>
				            <?php foreach ($catFuentesFinanciamiento->cat_insumos as $catInsumos): ?>
				            <tr>
				                <td><?= h($catInsumos->id) ?></td>
				                <td><?= h($catInsumos->co_usuario_id) ?></td>
				                <td><?= h($catInsumos->cat_fuentes_financiamiento_id) ?></td>
				                <td><?= h($catInsumos->cat_programa_id) ?></td>
				                <td><?= h($catInsumos->tipo_intervencion) ?></td>
				                <td><?= h($catInsumos->clave_intervencion) ?></td>
				                <td><?= h($catInsumos->nombre_intervencion) ?></td>
				                <td><?= h($catInsumos->clave_estrategia) ?></td>
				                <td><?= h($catInsumos->nombre_estrategia) ?></td>
				                <td><?= h($catInsumos->clave_linea_accion) ?></td>
				                <td><?= h($catInsumos->nombre_linea_accion) ?></td>
				                <td><?= h($catInsumos->clave_actividad) ?></td>
				                <td><?= h($catInsumos->nombre_actividad) ?></td>
				                <td><?= h($catInsumos->clave_accion_especifica) ?></td>
				                <td><?= h($catInsumos->indice_accion_especifica) ?></td>
				                <td><?= h($catInsumos->nombre_accion_especifica) ?></td>
				                <td><?= h($catInsumos->clave_partida) ?></td>
				                <td><?= h($catInsumos->nombre_partida) ?></td>
				                <td><?= h($catInsumos->clave_insumo) ?></td>
				                <td><?= h($catInsumos->insumo) ?></td>
				                <td><?= h($catInsumos->unidad_medida) ?></td>
				                <td><?= h($catInsumos->descripcion_insumo) ?></td>
				                <td><?= h($catInsumos->cantidad) ?></td>
				                <td><?= h($catInsumos->precio_unitario) ?></td>
				                <td><?= h($catInsumos->total) ?></td>
				                <td><?= h($catInsumos->estatus) ?></td>
				                <td><?= h($catInsumos->cat_estatu_id) ?></td>
				                <td><?= h($catInsumos->created) ?></td>
				                <td><?= h($catInsumos->modified) ?></td>
				                <td class="actions">
				                    <?= $this->Html->link(__('View'), ['controller' => 'CatInsumos', 'action' => 'view', $catInsumos->id]) ?>
				                    <?= $this->Html->link(__('Edit'), ['controller' => 'CatInsumos', 'action' => 'edit', $catInsumos->id]) ?>
				                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CatInsumos', 'action' => 'delete', $catInsumos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $catInsumos->id)]) ?>
				                </td>
				            </tr>
				            <?php endforeach; ?>
				        </table>
				        <?php endif; ?>
				    </div>
				            </div>
        </div>
	</div>
</div>
