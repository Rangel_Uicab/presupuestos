<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CI SESA:';
?>
<!DOCTYPE html>
<html class="no-js css-menubar">
<head>
    <?= $this->Html->charset() ?>

    <meta charset="utf-8">
<!--    <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?php echo $this->Html->css(
									[
									 'bootstrap/bootstrap.minfd53.css?v4.0.1',
									 'bootstrap/bootstrap-extend.minfd53.css?v4.0.1',
									 'core/site.minfd53.css?v4.0.1',
									 'core/skintools.minfd53.css?v4.0.1',
//								     'animsition/animsition.minfd53.css?v4.0.1',	
									 'asscrollable/asScrollable.minfd53.css?v4.0.1',
//									  'switchery/switchery.minfd53.css?v4.0.1',
//									  'intro-js/introjs.minfd53.css?v4.0.1',
									  'slidepanel/slidePanel.minfd53.css?v4.0.1',
//									  'flag-icon-css/flag-icon.minfd53.css?v4.0.1',
									  'waves/waves.minfd53.css?v4.0.1',
									  'material-design/material-design.minfd53.css?v4.0.1',
									  'brand-icons/brand-icons.minfd53.css?v4.0.1',         
									]
								) 
    ?>

    <?php 
    echo $this->Html->script(
							    [                                    
//                                    'core/skintools.minfd53.js?v4.0.1',
                                    'breakpoints/breakpoints.minfd53.js?v4.0.1',        
                                    'jquery/jquery.minfd53.js?v4.0.1',	    
							    ]
	                            ); 
    ?>
    
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <script> Breakpoints();</script>
    
</head>
<body class="animsition site-navbar-small dashboard">
  <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega navbar-inverse"
    role="navigation">

    <div class="navbar-header">

      <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
        data-toggle="collapse">

      </button>
      <a class="navbar-brand navbar-brand-center" href="#">
        <img class="navbar-brand-logo navbar-brand-logo-normal" src="<?php echo $this->request->webroot; ?>img/logo_pie.png"
          title="Remark">
        <img class="navbar-brand-logo navbar-brand-logo-special" src="<?php echo $this->request->webroot; ?>img/logo_pie.png"
          title="Remark">
        <span class="navbar-brand-text hidden-xs-down"> SISTEMA DE CONTROL DE VERIFICADORES Y NOTIFICADORES EN SALUD</span>
      </a>
      
    </div>
  </nav>

  <!-- Page -->
  <div class="page">
    <div class="page-content panel">

      <?= $this->Flash->render()?>
      <br>
      <?= $this->fetch('content') ?>
    </div>
  </div>
  <!-- End Page -->


  <!-- Footer -->

  <footer class="site-footer">
    <div class="site-footer-legal">© SISTEMA DE CONTROL DE VERIFICADORES</div>
    <div class="site-footer-right">
       COFEPRIS QROO <?php echo date('Y')?>
    </div>
  </footer>
 
 
 <?php
         echo $this->Html->script(
                                    [   
                                        'babel-external-helpers/babel-external-helpersfd53.js?v4.0.1',
                                        
                                        'popper-js/umd/popper.minfd53.js?v4.0.1',
                                        'bootstrap/bootstrap.minfd53.js?v4.0.1',
//                                      'animsition/animsition.minfd53.js?v4.0.1',
                                        'mousewheel/jquery.mousewheel.minfd53.js?v4.0.1',
                                        'asscrollbar/jquery-asScrollbar.minfd53.js?v4.0.1',
                                        'asscrollable/jquery-asScrollable.minfd53.js?v4.0.1',
                                        'ashoverscroll/jquery-asHoverScroll.minfd53.js?v4.0.1',
                                        'waves/waves.minfd53.js?v4.0.1',
//                                      'switchery/switchery.minfd53.js?v4.0.1',
//                                      'intro-js/intro.minfd53.js?v4.0.1',
//                                      'screenfull/screenfull.minfd53.js?v4.0.1',
                                        'slidepanel/jquery-slidePanel.minfd53.js?v4.0.1',
                                        'core/State.minfd53.js?v4.0.1',
                                        'core/Component.minfd53.js?v4.0.1',
                                        'core/Plugin.minfd53.js?v4.0.1',
                                        'core/Base.minfd53.js?v4.0.1',
                                        'core/Config.minfd53.js?v4.0.1',
                                        'Section/Menubar.minfd53.js?v4.0.1',
                                        'Section/Sidebar.minfd53.js?v4.0.1',
                                        'Section/PageAside.minfd53.js?v4.0.1',
                                        'Plugin/menu.minfd53.js?v4.0.1',
//                                      'config/colors.minfd53.js?v4.0.1',
//                                      'config/tour.minfd53.js?v4.0.1', 
                                        'core/Site.minfd53.js?v4.0.1',   
                                        'Plugin/asscrollable.minfd53.js?v4.0.1',
                                        'Plugin/slidepanel.minfd53.js?v4.0.1',
//                                        'Plugin/switchery.minfd53.js?v4.0.1',
//                                      'peity/jquery.peity.minfd53.js?v4.0.1',
                                        'core/v1.minfd53.js?v4.0.1',                                    
//                                      'core/BaseApp.minfd53',     
                                    ]
                                ); 
 ?>
  
</body>

</html>
