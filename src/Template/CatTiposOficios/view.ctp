<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row">
	<div class="col-lg-3">
		<div class="list-group">
			<a class="list-group-item active">Acciones</a>
			<?= $this->Html->link('<i class="icon md-format-list-bulleted">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			<?= $this->Html->link('<i class="icon md-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $catTiposOficio->id],['class'=>'list-group-item','escape'=>false]) ?>				
	        <?= $this->Form->postLink(__('<i class="icon md-delete">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $catTiposOficio->id],['confirm' => __('Realmente desea eliminar el registro con Id # {0}?', $catTiposOficio->id),'escape'=>false,'class'=>'list-group-item'])?>
	    </div>	
	</div>

	<div class="col-sm-9">
		<div class="panel panel-primary panel-line">
            <div class="panel-heading">
                <h2 class="panel-title">Informaci&oacute;n</h2>
            </div>
            <div class="panel-body">
             	<h3><?= h($catTiposOficio->name) ?></h3>
			    <table class="table table-condensed">
																        <tr>
				            <th scope="row"><?= __('Id') ?></th>
				            <td><?= h($catTiposOficio->id) ?></td>
				        </tr>
																        <tr>
				            <th scope="row"><?= __('Name') ?></th>
				            <td><?= h($catTiposOficio->name) ?></td>
				        </tr>
																																        <tr>
				            <th scope="row"><?= __('Created') ?></th>
				            <td><?= h($catTiposOficio->created) ?></td>
				        </tr>
								        <tr>
				            <th scope="row"><?= __('Modified') ?></th>
				            <td><?= h($catTiposOficio->modified) ?></td>
				        </tr>
																				        <tr>
				            <th scope="row"><?= __('Activo') ?></th>
				            <td><?= $catTiposOficio->activo ? '<span class="label label-success">SI</span>' : '<span class="label label-danger">NO</span>'; ?></td>
				        </tr>
												    </table>
												    <div class="related">
				        <h4><?= __('Related Of Oficios') ?></h4>
				        <?php if (!empty($catTiposOficio->of_oficios)): ?>
				        <table class="table table-bordered" cellpadding="0" cellspacing="0">
				            <tr>
								                <th scope="col"><?= __('Id') ?></th>
								                <th scope="col"><?= __('Cat Tipos Oficio Id') ?></th>
								                <th scope="col"><?= __('Cat Unidade Id') ?></th>
								                <th scope="col"><?= __('Co Usuario Id') ?></th>
								                <th scope="col"><?= __('Cat Programa Id') ?></th>
								                <th scope="col"><?= __('Num Oficio') ?></th>
								                <th scope="col"><?= __('Asunto') ?></th>
								                <th scope="col"><?= __('Fecha') ?></th>
								                <th scope="col"><?= __('Fecha Recepcion') ?></th>
								                <th scope="col"><?= __('Resumen') ?></th>
								                <th scope="col"><?= __('Activo') ?></th>
								                <th scope="col"><?= __('Created') ?></th>
								                <th scope="col"><?= __('Modified') ?></th>
								                <th scope="col" class="actions"><?= __('Actions') ?></th>
				            </tr>
				            <?php foreach ($catTiposOficio->of_oficios as $ofOficios): ?>
				            <tr>
				                <td><?= h($ofOficios->id) ?></td>
				                <td><?= h($ofOficios->cat_tipos_oficio_id) ?></td>
				                <td><?= h($ofOficios->cat_unidade_id) ?></td>
				                <td><?= h($ofOficios->co_usuario_id) ?></td>
				                <td><?= h($ofOficios->cat_programa_id) ?></td>
				                <td><?= h($ofOficios->num_oficio) ?></td>
				                <td><?= h($ofOficios->asunto) ?></td>
				                <td><?= h($ofOficios->fecha) ?></td>
				                <td><?= h($ofOficios->fecha_recepcion) ?></td>
				                <td><?= h($ofOficios->resumen) ?></td>
				                <td><?= h($ofOficios->activo) ?></td>
				                <td><?= h($ofOficios->created) ?></td>
				                <td><?= h($ofOficios->modified) ?></td>
				                <td class="actions">
				                    <?= $this->Html->link(__('View'), ['controller' => 'OfOficios', 'action' => 'view', $ofOficios->id]) ?>
				                    <?= $this->Html->link(__('Edit'), ['controller' => 'OfOficios', 'action' => 'edit', $ofOficios->id]) ?>
				                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'OfOficios', 'action' => 'delete', $ofOficios->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ofOficios->id)]) ?>
				                </td>
				            </tr>
				            <?php endforeach; ?>
				        </table>
				        <?php endif; ?>
				    </div>
				            </div>
        </div>
	</div>
</div>
