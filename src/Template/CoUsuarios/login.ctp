
	 <div class="brand">
		<img class="brand-img" src="<?php echo $this->request->webroot; ?>img/logo.png" alt="..." width="300">
        <h3 class="brand-text">DIRECCI&Oacute;N DE SERVICIOS DE SALUD</h>
      </div>
		
		<p>UNIDAD DE AN&Aacute;LISIS, SEGUIMIENTO Y <br> ENLACE SIAFFASPE</p>
              
          <?php
            $templatesUsername = [
                                    'input' => '
                                                <div class="form-group form-material floating" data-plugin="formMaterial">                                
                                                    <input autocomplete = "off" class="form-control" type="{{type}}" name="{{name}}"{{attrs}}/>
                                                    <label  class="floating-label">Usuario</label>
                                                </div>',
                                    'inputContainer' => '{{content}}',
                                ];


            $myTemplatesPassword = [
                                    'input' => '
                                                <div class="form-group form-material floating" data-plugin="formMaterial">
                                                 <input class="form-control" type="{{type}}" name="{{name}}"{{attrs}}/>
                                                    <label class="floating-label">Clave de Acceso</label>
                                                       
                                                </div>',
                                    'inputContainer' => '{{content}}',
                                    ];
            ?>
          
          
          
           <?= $this->Form->create(null,['role'=>'form','autocomplete'=>"off"]) ?>
             <?php 
                echo $this->Form->input('login',
                                                [
                                                'type'=>'text',
                                                'label'=>false,
                                                'templates'=>$templatesUsername,
                                                ]
                                            );
                echo $this->Form->input('password',
                                                [
                                                    'type'=>'password',
                                                    'label'=>false,
                                                    'templates'=> $myTemplatesPassword
                                                ]
                                       );
                echo $this->Form->button(__('Entrar'),['class'=>'btn btn-primary btn-block waves-effect waves-classic']);
                echo $this->Flash->render('flash');
            ?>
           <?= $this->Form->end()?>
           

<script type="text/javascript">
    var Login = document.getElementById("login").value;
    if(Login == "")
    {
        document.getElementById("login").focus();
    }
    else
    {
        document.getElementById("password").focus();
    }
</script>
