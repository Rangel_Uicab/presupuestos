<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row">
	<div class="col-lg-3">
		<div class="list-group">
			<a class="list-group-item active">Acciones</a>
			<?= $this->Html->link('<i class="icon md-format-list-bulleted">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			<?= $this->Html->link('<i class="icon md-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $catDireccione->id],['class'=>'list-group-item','escape'=>false]) ?>				
	        <?= $this->Form->postLink(__('<i class="icon md-delete">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $catDireccione->id],['confirm' => __('Realmente desea eliminar el registro con Id # {0}?', $catDireccione->id),'escape'=>false,'class'=>'list-group-item'])?>
	    </div>	
	</div>

	<div class="col-sm-9">
		<div class="panel panel-primary panel-line">
            <div class="panel-heading">
                <h2 class="panel-title">Informaci&oacute;n</h2>
            </div>
            <div class="panel-body">
             	<h3><?= h($catDireccione->name) ?></h3>
			    <table class="table table-condensed">
																        <tr>
				            <th scope="row"><?= __('Id') ?></th>
				            <td><?= h($catDireccione->id) ?></td>
				        </tr>
																        <tr>
				            <th scope="row"><?= __('Name') ?></th>
				            <td><?= h($catDireccione->name) ?></td>
				        </tr>
																																        <tr>
				            <th scope="row"><?= __('Created') ?></th>
				            <td><?= h($catDireccione->created) ?></td>
				        </tr>
								        <tr>
				            <th scope="row"><?= __('Modified') ?></th>
				            <td><?= h($catDireccione->modified) ?></td>
				        </tr>
																				        <tr>
				            <th scope="row"><?= __('Activo') ?></th>
				            <td><?= $catDireccione->activo ? '<span class="label label-success">SI</span>' : '<span class="label label-danger">NO</span>'; ?></td>
				        </tr>
												    </table>
												    <div class="related">
				        <h4><?= __('Related Cat Unidades') ?></h4>
				        <?php if (!empty($catDireccione->cat_unidades)): ?>
				        <table class="table table-bordered" cellpadding="0" cellspacing="0">
				            <tr>
								                <th scope="col"><?= __('Id') ?></th>
								                <th scope="col"><?= __('Cat Direccione Id') ?></th>
								                <th scope="col"><?= __('Cat Unidad Padre Id') ?></th>
								                <th scope="col"><?= __('Created') ?></th>
								                <th scope="col"><?= __('Name') ?></th>
								                <th scope="col"><?= __('Nombre Corto') ?></th>
								                <th scope="col"><?= __('Modified') ?></th>
								                <th scope="col" class="actions"><?= __('Actions') ?></th>
				            </tr>
				            <?php foreach ($catDireccione->cat_unidades as $catUnidades): ?>
				            <tr>
				                <td><?= h($catUnidades->id) ?></td>
				                <td><?= h($catUnidades->cat_direccione_id) ?></td>
				                <td><?= h($catUnidades->cat_unidad_padre_id) ?></td>
				                <td><?= h($catUnidades->created) ?></td>
				                <td><?= h($catUnidades->name) ?></td>
				                <td><?= h($catUnidades->nombre_corto) ?></td>
				                <td><?= h($catUnidades->modified) ?></td>
				                <td class="actions">
				                    <?= $this->Html->link(__('View'), ['controller' => 'CatUnidades', 'action' => 'view', $catUnidades->id]) ?>
				                    <?= $this->Html->link(__('Edit'), ['controller' => 'CatUnidades', 'action' => 'edit', $catUnidades->id]) ?>
				                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CatUnidades', 'action' => 'delete', $catUnidades->id], ['confirm' => __('Are you sure you want to delete # {0}?', $catUnidades->id)]) ?>
				                </td>
				            </tr>
				            <?php endforeach; ?>
				        </table>
				        <?php endif; ?>
				    </div>
				            </div>
        </div>
	</div>
</div>
