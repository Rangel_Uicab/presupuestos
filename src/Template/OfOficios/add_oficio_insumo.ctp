<?php
/**
  * @var \App\View\AppView $this
  */
?>
<script type="text/javascript">
var statSend = false;
function checkSubmit()
{
    if (!statSend)
    {
        statSend = true;
        document.getElementById('btnGuardar').disabled = true;
        return true;
    }
    else
    {
        alert("El formulario ya se esta enviando...");
        return false;
    }
}
</script>
<?php
	echo $this->Html->css('select2/select2.minfd53',['block'=>true]);
	echo $this->Html->script('select2/select2.full.minfd53',['block'=>true]);
?>
<?php
$this->loadHelper('Form', ['templates' => 'app_form']);
?>

<?php
$Meses = array(
		'01'=>'Enero',
		'02'=>'Febrero',
		'03'=>'Marzo',
		'04'=>'Abril',
		'05'=>'Mayo',
		'06'=>'Junio',
		'07'=>'Julio',
		'08'=>'Agosto',
		'09'=>'Septiembre',
		'10'=>'Octubre',
		'11'=>'Noviembre',
		'12'=>'Diciembre',
	);
?>

<div class="row">
	<div class="col-lg-5">
			<div class="list-group">
				<a class="list-group-item active">Acciones</a>
				<?= $this->Html->link('<i class="icon md-format-list-bulleted">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			</div>
      <hr>
      <div class="col-md-12" id="DivInsumo">

      </div>
	</div>

	<div class="col-lg-7">
    	<div class="panel panel-primary panel-line">
	        <div class="panel-heading">
	            <h2 class="panel-title"> <?= __('Nuevo Oficio') ?></h2>
	        </div>
	        <div class="panel-body">
        		<?= $this->Form->create($ofOficio,['role'=>'form','onsubmit'=>'return checkSubmit();']) ?>
        	    <div class="row">
					<div class="col-md-8">
						<?php
							echo $this->Form->control('cat_unidade_id', ['empty'=>true,'options' => $catUnidades,'label'=>['text'=>'Unidad']]);
						?>
					</div>
					<div class="col-md-4">
						<?php
							echo $this->Form->control('cat_programa_id', ['required'=>true,'empty'=>true,'label'=>['text'=>'Programa']]);
						?>
					</div>
          <div class="col-md-12">
						<?php
							echo $this->Form->control('cat_tipos_oficio_id', ['empty'=>false,'label'=>['text'=>'Tipo de Oficio']]);
						?>
					</div>
					<div class="col-md-4">
						<?php
							echo $this->Form->control('num_oficio', ['label'=>[]]);
						?>
					</div>
					<div class="col-md-8">
						<?php
							echo $this->Form->control('asunto', ['label'=>[]]);
						?>
					</div>
					<div class="col-md-6">
						<label>Fecha Oficio</label><br/>
						<div class="form-group form-inline">
							<?php
              if(!$this->request->session()->check('OficiosData'))
              {
								$this->request->data['fecha']['year'] = date('Y');
								$this->request->data['fecha']['month'] = date('m');
								$this->request->data['fecha']['day'] = date('d');
              }
              ?>
							<?php echo $this->Form->day('fecha',['label'=>['text'=>'Fecha']]);?>
							<?php echo $this->Form->month('fecha',['options'=>$Meses]);?>
							<?php echo $this->Form->year('fecha',['maxYear' => date('Y'),'minYear' => date('Y') - 1,'label'=>['text'=>'Año']]);?>
						</div>
					</div>
					<div class="col-md-6">
						<label>Fecha Recepci&oacute;n Oficio</label><br/>
						<div class="form-group form-inline">
							<?php
              if(!$this->request->session()->check('OficiosData'))
              {
                $this->request->data['fecha_recepcion']['year'] = date('Y');
                $this->request->data['fecha_recepcion']['month'] = date('m');
                $this->request->data['fecha_recepcion']['day'] = date('d');
              }
							?>
							<?php echo $this->Form->day('fecha_recepcion',['label'=>['text'=>'Fecha']]);?>
							<?php echo $this->Form->month('fecha_recepcion',['options'=>$Meses/*,'required'=>true*/]);?>
							<?php echo $this->Form->year('fecha_recepcion',['maxYear' => date('Y'),'minYear' => date('Y') - 1,'label'=>['text'=>'Año']]);?>
						</div>
					</div>
					<div class="col-md-12">
						<?php
							echo $this->Form->control('resumen', ['rows'=>2,'label'=>[]]);
						?>
					</div>
					<div class="col-md-12">
						<?php
							echo $this->Form->control('of_oficios_insumos.0.cat_insumo_id', ['rows'=>2,'label'=>['text'=>'Insumos']]);
						?>
					</div>
					<div class="col-md-6">
						<?php
							echo $this->Form->control('of_oficios_insumos.0.cantidad', ['label'=>[]]);
						?>
					</div>
					<div class="col-md-6">
						<?php
							echo $this->Form->control('of_oficios_insumos.0.precio_unitario', ['label'=>[]]);
						?>
					</div>
				</div>

       			<?= $this->Form->button('Guardar',array('type'=>'submit','class'=>'btn btn-primary waves-effect','id'=>'btnGuardar')) ?>

        		<?= $this->Form->end() ?>
    		</div>
	    </div>
	</div>
</div>

<script type="text/javascript">
$("#cat-unidade-id").select2	({placeholder: "SELECCIONAR",allowClear: true});
$("#of-oficios-insumos-0-cat-insumo-id").select2	({placeholder: "SELECCIONAR",allowClear: true});
</script>

<script type="text/javascript">
$('#of-oficios-insumos-0-cat-insumo-id').on('change',function ()
{
      var id = $(this).val();
      if(id)
      {
            $.ajax({
              type:'post',
              async:true,
              url:"<?php echo $this->Url->build(['controller'=>'cat_insumos','action'=>'get_info_insumo']) ?>",
              beforeSend:function () {
                //$('#loading').css({'display':'inline'});
              },
              data:{id:id}
            }).done(function (html) {
              $("#DivInsumo").html(html);
            }).fail(function (error) {
              console.log(error);
            }).always(function () {
            //	$('#loading').css({'display':'none'});
            })
      }
  else
  {
    alert('DEBE SELECCIONAR EL INSUMO');
  }

});
</script>
<script type="text/javascript">

	 $("#cat-programa-id").on('change',function()
        {
            var id = $(this).val();
            $("#of-oficios-insumos-0-cat-insumo-id").find('option').remove();
            if (id)
            {
    //      document.getElementById('cargando').style.display='block';
                var dataString = {id:id};
                $.ajax(
                    {
                        type: "POST",
                        url: "<?php echo $this->Url->build(['controller'=>'cat_insumos','action' => 'get_insumos_por_programa','_ext'=>'json']); ?>" ,
                        dataType: "json",
                        data: dataString,
                        cache: false,
                        success: function(html)
                        {
    //              document.getElementById('cargando').style.display='none';
                            $('<option>').val('').text('').appendTo($("#of-oficios-insumos-0-cat-insumo-id"));
                            $.each(html.catInsumos, function(key, value)
                            {
                                $('<option>').val(key).text(value).appendTo($("#of-oficios-insumos-0-cat-insumo-id"));
                            });
                        }
                    });
            }
        });

</script>
<script type="text/javascript">

	 $("#cat-unidade-id").on('change',function()
        {
            var id = $(this).val();
              $("#cat-programa-id").find('option').remove();
            if (id)
            {
    //      document.getElementById('cargando').style.display='block';
                var dataString = {cat_unidade_id:id};
                $.ajax(
                    {
                        type: "POST",
                        url: "<?php echo $this->Url->build(['controller'=>'cat_programas','action' => 'get_programas_por_departamento','_ext'=>'json']); ?>" ,
                        dataType: "json",
                        data: dataString,
                        cache: false,
                        success: function(html)
                        {
    //              document.getElementById('cargando').style.display='none';
                            $('<option>').val('').text('').appendTo($("#cat-programa-id"));
                            $.each(html.catProgramas, function(key, value)
                            {
                                $('<option>').val(key).text(value).appendTo($("#cat-programa-id"));
                            });
                        }
                    });
            }
        });

</script>
<script type="text/javascript">
    $('#cat-programa-id').on('change',function ()
    {
        var cat_programa_id = $(this).val();
        if(cat_programa_id)
        {
            $.ajax
            (
                {
                    type:'post',
                    url:"<?php echo $this->Url->build(['controller'=>'cat_programas','action'=>'get_name_oficio','_ext'=>'json']) ?>",
                    data:{
                        cat_programa_id:cat_programa_id
                    }
                }
            ).done(function (data)
            {
                console.log(data.catPrograma.name_oficio);
                $('#num-oficio').val(data.catPrograma.name_oficio);
            })
            .error(function (error) {
                console.log(error);
            });
        }
        else
        {
            $('#num-oficio').val('');
        }
    });
</script>
