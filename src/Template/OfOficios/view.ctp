<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row">
	<div class="col-lg-3">
		<div class="list-group">
			<a class="list-group-item active">Acciones</a>
			<?= $this->Html->link('<i class="icon md-format-list-bulleted">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			<?= $this->Html->link('<i class="icon md-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $ofOficio->id],['class'=>'list-group-item','escape'=>false]) ?>				
	        <?= $this->Form->postLink(__('<i class="icon md-delete">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $ofOficio->id],['confirm' => __('Realmente desea eliminar el registro con Id # {0}?', $ofOficio->id),'escape'=>false,'class'=>'list-group-item'])?>
	    </div>	
	</div>

	<div class="col-sm-9">
		<div class="panel panel-primary panel-line">
            <div class="panel-heading">
                <h2 class="panel-title">Informaci&oacute;n</h2>
            </div>
            <div class="panel-body">
             	<h3><?= h($ofOficio->id) ?></h3>
			    <table class="table table-condensed">
																        <tr>
				            <th scope="row"><?= __('Id') ?></th>
				            <td><?= h($ofOficio->id) ?></td>
				        </tr>
														<tr>
				            <th scope="row"><?= __('Cat Unidade') ?></th>
				            <td><?= $ofOficio->has('cat_unidade') ? $this->Html->link($ofOficio->cat_unidade->name, ['controller' => 'CatUnidades', 'action' => 'view', $ofOficio->cat_unidade->id]) : '' ?></td>
				        </tr>
														<tr>
				            <th scope="row"><?= __('Co Usuario') ?></th>
				            <td><?= $ofOficio->has('co_usuario') ? $this->Html->link($ofOficio->co_usuario->nombre_completo, ['controller' => 'CoUsuarios', 'action' => 'view', $ofOficio->co_usuario->id]) : '' ?></td>
				        </tr>
																        <tr>
				            <th scope="row"><?= __('Num Oficio') ?></th>
				            <td><?= h($ofOficio->num_oficio) ?></td>
				        </tr>
																        <tr>
				            <th scope="row"><?= __('Asunto') ?></th>
				            <td><?= h($ofOficio->asunto) ?></td>
				        </tr>
																																        <tr>
				            <th scope="row"><?= __('Fecha') ?></th>
				            <td><?= h($ofOficio->fecha) ?></td>
				        </tr>
								        <tr>
				            <th scope="row"><?= __('Fecha Recepcion') ?></th>
				            <td><?= h($ofOficio->fecha_recepcion) ?></td>
				        </tr>
								        <tr>
				            <th scope="row"><?= __('Created') ?></th>
				            <td><?= h($ofOficio->created) ?></td>
				        </tr>
								        <tr>
				            <th scope="row"><?= __('Modified') ?></th>
				            <td><?= h($ofOficio->modified) ?></td>
				        </tr>
																				        <tr>
				            <th scope="row"><?= __('Activo') ?></th>
				            <td><?= $ofOficio->activo ? '<span class="label label-success">SI</span>' : '<span class="label label-danger">NO</span>'; ?></td>
				        </tr>
												    </table>
												    <div class="row">
				        <h4><?= __('Resumen') ?></h4>
				        <?= $this->Text->autoParagraph(h($ofOficio->resumen)); ?>
				    </div>
																    <div class="related">
				        <h4><?= __('Related Of Oficios Insumos') ?></h4>
				        <?php if (!empty($ofOficio->of_oficios_insumos)): ?>
				        <table class="table table-bordered" cellpadding="0" cellspacing="0">
				            <tr>
								                <th scope="col"><?= __('Id') ?></th>
								                <th scope="col"><?= __('Co Usuario Id') ?></th>
								                <th scope="col"><?= __('Of Oficio Id') ?></th>
								                <th scope="col"><?= __('Cat Insumo Id') ?></th>
								                <th scope="col"><?= __('Fecha Entrega') ?></th>
								                <th scope="col"><?= __('Cantidad') ?></th>
								                <th scope="col"><?= __('Precio Unitario') ?></th>
								                <th scope="col"><?= __('Created') ?></th>
								                <th scope="col"><?= __('Modified') ?></th>
								                <th scope="col" class="actions"><?= __('Actions') ?></th>
				            </tr>
				            <?php foreach ($ofOficio->of_oficios_insumos as $ofOficiosInsumos): ?>
				            <tr>
				                <td><?= h($ofOficiosInsumos->id) ?></td>
				                <td><?= h($ofOficiosInsumos->co_usuario_id) ?></td>
				                <td><?= h($ofOficiosInsumos->of_oficio_id) ?></td>
				                <td><?= h($ofOficiosInsumos->cat_insumo_id) ?></td>
				                <td><?= h($ofOficiosInsumos->fecha_entrega) ?></td>
				                <td><?= h($ofOficiosInsumos->cantidad) ?></td>
				                <td><?= h($ofOficiosInsumos->precio_unitario) ?></td>
				                <td><?= h($ofOficiosInsumos->created) ?></td>
				                <td><?= h($ofOficiosInsumos->modified) ?></td>
				                <td class="actions">
				                    <?= $this->Html->link(__('View'), ['controller' => 'OfOficiosInsumos', 'action' => 'view', $ofOficiosInsumos->id]) ?>
				                    <?= $this->Html->link(__('Edit'), ['controller' => 'OfOficiosInsumos', 'action' => 'edit', $ofOficiosInsumos->id]) ?>
				                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'OfOficiosInsumos', 'action' => 'delete', $ofOficiosInsumos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ofOficiosInsumos->id)]) ?>
				                </td>
				            </tr>
				            <?php endforeach; ?>
				        </table>
				        <?php endif; ?>
				    </div>
				            </div>
        </div>
	</div>
</div>
