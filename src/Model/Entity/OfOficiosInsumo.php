<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OfOficiosInsumo Entity
 *
 * @property string $id
 * @property string $co_usuario_id
 * @property string $of_oficio_id
 * @property string $cat_insumo_id
 * @property \Cake\I18n\FrozenDate $fecha_entrega
 * @property int $cantidad
 * @property float $precio_unitario
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\CoUsuario $co_usuario
 * @property \App\Model\Entity\OfOficio $of_oficio
 * @property \App\Model\Entity\CatInsumo $cat_insumo
 */
class OfOficiosInsumo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
