<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OfOficio Entity
 *
 * @property string $id
 * @property string $cat_unidade_id
 * @property string $co_usuario_id
 * @property string $num_oficio
 * @property string $asunto
 * @property \Cake\I18n\FrozenDate $fecha
 * @property \Cake\I18n\FrozenDate $fecha_recepcion
 * @property string $resumen
 * @property bool $activo
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\CatUnidade $cat_unidade
 * @property \App\Model\Entity\CoUsuario $co_usuario
 * @property \App\Model\Entity\OfOficiosInsumo[] $of_oficios_insumos
 */
class OfOficio extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    protected $_virtual = ['name_completo'];

    protected function _getNameCompleto()
    {
        return $this->_properties['num_oficio'].' - '. $this->_properties['asunto'].' '.$this->_properties['resumen'];
    }

}
