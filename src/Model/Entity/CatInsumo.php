<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
/**
 * CatInsumo Entity
 *
 * @property string $id
 * @property string $co_usuario_id
 * @property string $cat_fuentes_financiamiento_id
 * @property string $cat_programa_id
 * @property string $tipo_intervencion
 * @property int $clave_intervencion
 * @property string $nombre_intervencion
 * @property int $clave_estrategia
 * @property string $nombre_estrategia
 * @property int $clave_linea_accion
 * @property string $nombre_linea_accion
 * @property int $clave_actividad
 * @property string $nombre_actividad
 * @property int $clave_accion_especifica
 * @property string $indice_accion_especifica
 * @property string $nombre_accion_especifica
 * @property int $clave_partida
 * @property string $nombre_partida
 * @property string $clave_insumo
 * @property string $insumo
 * @property string $unidad_medida
 * @property string $descripcion_insumo
 * @property int $cantidad
 * @property float $precio_unitario
 * @property float $total
 * @property string $estatus
 * @property int $cat_estatu_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\CoUsuario $co_usuario
 * @property \App\Model\Entity\CatFuentesFinanciamiento $cat_fuentes_financiamiento
 * @property \App\Model\Entity\CatPrograma $cat_programa
 * @property \App\Model\Entity\CatEstatus $cat_estatus
 * @property \App\Model\Entity\OfOficiosInsumo[] $of_oficios_insumos
 */
class CatInsumo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected $_virtual = ['total_oficios','total_compra','name_completo'];

    protected function _getNameCompleto()
    {
        return $this->_properties['insumo'].' $ '.number_format($this->_properties['total'],2);
    }

    protected function _getTotalOficios()
    {
        $TotalOficios = TableRegistry::get('OfOficiosInsumos')->find('all')->where(['OfOficiosInsumos.cat_insumo_id'=>$this->_properties['id']])->count();
        return $TotalOficios;
    }

    protected function _getTotalCompra()
    {
      $totalCompra = TableRegistry::get('OfOficiosInsumos')->find('all')
                                                      ->select(['total_compra' => TableRegistry::get('OfOficiosInsumos')->find()->func()->sum('(OfOficiosInsumos.cantidad) * (OfOficiosInsumos.precio_unitario)')])
                                                      ->where(['OfOficiosInsumos.cat_insumo_id' => $this->_properties['id']])
                                                      ->first();

      return $totalCompra->total_compra;
    }

}
