<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CatPrograma Entity
 *
 * @property string $id
 * @property string $cat_unidade_id
 * @property string $name
 * @property bool $activo
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\CatUnidade $cat_unidade
 * @property \App\Model\Entity\CatInsumo[] $cat_insumos
 */
class CatPrograma extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
