<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OfOficios Model
 *
 * @property \App\Model\Table\CatUnidadesTable|\Cake\ORM\Association\BelongsTo $CatUnidades
 * @property \App\Model\Table\CoUsuariosTable|\Cake\ORM\Association\BelongsTo $CoUsuarios
 * @property \App\Model\Table\OfOficiosInsumosTable|\Cake\ORM\Association\HasMany $OfOficiosInsumos
 *
 * @method \App\Model\Entity\OfOficio get($primaryKey, $options = [])
 * @method \App\Model\Entity\OfOficio newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OfOficio[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OfOficio|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OfOficio patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OfOficio[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OfOficio findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OfOficiosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('of_oficios');
        $this->setDisplayField('name_completo');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('CatUnidades', [
            'foreignKey' => 'cat_unidade_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CoUsuarios', [
            'foreignKey' => 'co_usuario_id',
            'joinType' => 'INNER'
        ]);
		    $this->belongsTo('CatProgramas', [
            'foreignKey' => 'cat_programa_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('CatTiposOficios', [
            'foreignKey' => 'cat_tipos_oficio_id',
            'joinType' => 'LEFT'
        ]);
        $this->hasMany('OfOficiosInsumos', [
            'foreignKey' => 'of_oficio_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('num_oficio', 'create')
            ->notEmpty('num_oficio');

        $validator
            ->requirePresence('asunto', 'create')
            ->notEmpty('asunto');

        $validator
            ->date('fecha')
            ->requirePresence('fecha', 'create')
            ->notEmpty('fecha');

        $validator
            ->date('fecha_recepcion')
            ->allowEmpty('fecha_recepcion');

        $validator
            ->allowEmpty('resumen');

        $validator
            ->boolean('activo')
            ->allowEmpty('activo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cat_unidade_id'], 'CatUnidades'));
        $rules->add($rules->existsIn(['co_usuario_id'], 'CoUsuarios'));

        return $rules;
    }
}
