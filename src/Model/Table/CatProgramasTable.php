<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatProgramas Model
 *
 * @property \App\Model\Table\CatUnidadesTable|\Cake\ORM\Association\BelongsTo $CatUnidades
 * @property \App\Model\Table\CatInsumosTable|\Cake\ORM\Association\HasMany $CatInsumos
 *
 * @method \App\Model\Entity\CatPrograma get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatPrograma newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatPrograma[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatPrograma|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatPrograma patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatPrograma[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatPrograma findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CatProgramasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cat_programas');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('CatUnidades', [
            'foreignKey' => 'cat_unidade_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('CatInsumos', [
            'foreignKey' => 'cat_programa_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->boolean('activo')
            ->allowEmpty('activo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cat_unidade_id'], 'CatUnidades'));

        return $rules;
    }
}
