<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatDirecciones Model
 *
 * @property \App\Model\Table\CatUnidadesTable|\Cake\ORM\Association\HasMany $CatUnidades
 *
 * @method \App\Model\Entity\CatDireccione get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatDireccione newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatDireccione[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatDireccione|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatDireccione patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatDireccione[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatDireccione findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CatDireccionesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cat_direcciones');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('CatUnidades', [
            'foreignKey' => 'cat_direccione_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->boolean('activo')
            ->allowEmpty('activo');

        return $validator;
    }
}
