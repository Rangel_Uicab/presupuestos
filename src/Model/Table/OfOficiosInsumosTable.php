<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OfOficiosInsumos Model
 *
 * @property \App\Model\Table\CoUsuariosTable|\Cake\ORM\Association\BelongsTo $CoUsuarios
 * @property \App\Model\Table\OfOficiosTable|\Cake\ORM\Association\BelongsTo $OfOficios
 * @property \App\Model\Table\CatInsumosTable|\Cake\ORM\Association\BelongsTo $CatInsumos
 *
 * @method \App\Model\Entity\OfOficiosInsumo get($primaryKey, $options = [])
 * @method \App\Model\Entity\OfOficiosInsumo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OfOficiosInsumo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OfOficiosInsumo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OfOficiosInsumo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OfOficiosInsumo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OfOficiosInsumo findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OfOficiosInsumosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('of_oficios_insumos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('CoUsuarios', [
            'foreignKey' => 'co_usuario_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('OfOficios', [
            'foreignKey' => 'of_oficio_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CatInsumos', [
            'foreignKey' => 'cat_insumo_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('fecha_entrega')
            ->allowEmpty('fecha_entrega');

        $validator
            ->integer('cantidad')
            ->allowEmpty('cantidad');

        $validator
            ->numeric('precio_unitario')
            ->allowEmpty('precio_unitario');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['co_usuario_id'], 'CoUsuarios'));
        $rules->add($rules->existsIn(['of_oficio_id'], 'OfOficios'));
        $rules->add($rules->existsIn(['cat_insumo_id'], 'CatInsumos'));

        return $rules;
    }
}
