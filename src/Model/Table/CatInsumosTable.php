<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatInsumos Model
 *
 * @property \App\Model\Table\CoUsuariosTable|\Cake\ORM\Association\BelongsTo $CoUsuarios
 * @property \App\Model\Table\CatFuentesFinanciamientosTable|\Cake\ORM\Association\BelongsTo $CatFuentesFinanciamientos
 * @property \App\Model\Table\CatProgramasTable|\Cake\ORM\Association\BelongsTo $CatProgramas
 * @property \App\Model\Table\CatEstatusTable|\Cake\ORM\Association\BelongsTo $CatEstatus
 * @property \App\Model\Table\OfOficiosInsumosTable|\Cake\ORM\Association\HasMany $OfOficiosInsumos
 *
 * @method \App\Model\Entity\CatInsumo get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatInsumo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatInsumo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatInsumo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatInsumo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatInsumo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatInsumo findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CatInsumosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cat_insumos');
        $this->setDisplayField('name_completo');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('CoUsuarios', [
            'foreignKey' => 'co_usuario_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CoUsuariosEliminados', [
            'foreignKey' => 'co_usuario_elimino_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('CatFuentesFinanciamientos', [
            'foreignKey' => 'cat_fuentes_financiamiento_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CatProgramas', [
            'foreignKey' => 'cat_programa_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CatEstatus', [
            'foreignKey' => 'cat_estatu_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('OfOficiosInsumos', [
            'foreignKey' => 'cat_insumo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('tipo_intervencion');

        $validator
            ->integer('clave_intervencion')
            ->allowEmpty('clave_intervencion');

        $validator
            ->allowEmpty('nombre_intervencion');

        $validator
            ->integer('clave_estrategia')
            ->allowEmpty('clave_estrategia');

        $validator
            ->allowEmpty('nombre_estrategia');

        $validator
            ->integer('clave_linea_accion')
            ->allowEmpty('clave_linea_accion');

        $validator
            ->allowEmpty('nombre_linea_accion');

        $validator
            ->integer('clave_actividad')
            ->allowEmpty('clave_actividad');

        $validator
            ->allowEmpty('nombre_actividad');

        $validator
            ->integer('clave_accion_especifica')
            ->allowEmpty('clave_accion_especifica');

        $validator
            ->allowEmpty('indice_accion_especifica');

        $validator
            ->allowEmpty('nombre_accion_especifica');

        $validator
            ->integer('clave_partida')
            ->allowEmpty('clave_partida');

        $validator
            ->allowEmpty('nombre_partida');

        $validator
            ->allowEmpty('clave_insumo');

        $validator
            ->requirePresence('insumo', 'create')
            ->notEmpty('insumo');

        $validator
            ->allowEmpty('unidad_medida');

        $validator
            ->allowEmpty('descripcion_insumo');

        $validator
            ->integer('cantidad')
            ->allowEmpty('cantidad');

        $validator
            ->numeric('precio_unitario')
            ->allowEmpty('precio_unitario');

        $validator
            ->numeric('total')
            ->allowEmpty('total');

        $validator
            ->allowEmpty('estatus');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['co_usuario_id'], 'CoUsuarios'));
        $rules->add($rules->existsIn(['cat_fuentes_financiamiento_id'], 'CatFuentesFinanciamientos'));
        $rules->add($rules->existsIn(['cat_programa_id'], 'CatProgramas'));
        $rules->add($rules->existsIn(['cat_estatu_id'], 'CatEstatus'));

        return $rules;
    }
}
