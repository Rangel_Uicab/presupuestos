<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatFuentesFinanciamientos Model
 *
 * @property \App\Model\Table\CatInsumosTable|\Cake\ORM\Association\HasMany $CatInsumos
 *
 * @method \App\Model\Entity\CatFuentesFinanciamiento get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatFuentesFinanciamiento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatFuentesFinanciamiento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatFuentesFinanciamiento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatFuentesFinanciamiento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatFuentesFinanciamiento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatFuentesFinanciamiento findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CatFuentesFinanciamientosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cat_fuentes_financiamientos');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('CatInsumos', [
            'foreignKey' => 'cat_fuentes_financiamiento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->boolean('activo')
            ->allowEmpty('activo');

        return $validator;
    }
}
