<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatUnidades Model
 *
 * @property \App\Model\Table\CatDireccionesTable|\Cake\ORM\Association\BelongsTo $CatDirecciones
 * @property \App\Model\Table\CatUnidadesTable|\Cake\ORM\Association\BelongsTo $CatUnidades
 * @property \App\Model\Table\CatProgramasTable|\Cake\ORM\Association\HasMany $CatProgramas
 * @property \App\Model\Table\CoUsuariosTable|\Cake\ORM\Association\HasMany $CoUsuarios
 * @property \App\Model\Table\OfOficiosTable|\Cake\ORM\Association\HasMany $OfOficios
 *
 * @method \App\Model\Entity\CatUnidade get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatUnidade newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatUnidade[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatUnidade|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatUnidade patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatUnidade[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatUnidade findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CatUnidadesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cat_unidades');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('CatDirecciones', [
            'foreignKey' => 'cat_direccione_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CatUnidadesPadres', [
            'className' => 'CatUnidades',
            'foreignKey' => 'cat_unidad_padre_id'
        ]);
        $this->hasMany('CatProgramas', [
            'foreignKey' => 'cat_unidade_id'
        ]);
        $this->hasMany('CoUsuarios', [
            'foreignKey' => 'cat_unidade_id'
        ]);
        $this->hasMany('OfOficios', [
            'foreignKey' => 'cat_unidade_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('nombre_corto', 'create')
            ->notEmpty('nombre_corto');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cat_direccione_id'], 'CatDirecciones'));
        //$rules->add($rules->existsIn(['cat_unidad_padre_id'], 'CatUnidades'));

        return $rules;
    }
}
