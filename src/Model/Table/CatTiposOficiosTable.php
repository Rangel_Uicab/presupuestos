<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatTiposOficios Model
 *
 * @property \App\Model\Table\OfOficiosTable|\Cake\ORM\Association\HasMany $OfOficios
 *
 * @method \App\Model\Entity\CatTiposOficio get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatTiposOficio newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatTiposOficio[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatTiposOficio|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatTiposOficio patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatTiposOficio[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatTiposOficio findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CatTiposOficiosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cat_tipos_oficios');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('OfOficios', [
            'foreignKey' => 'cat_tipos_oficio_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->boolean('activo')
            ->allowEmpty('activo');

        return $validator;
    }
}
